<?php
/**
 * Template Name: Full Width
 */

get_header(); ?>
    <div class="top-service">
        <?php the_post_thumbnail(); ?>
    </div>

    <h1 class="service-h1">Сервисное обслуживание оборудования</h1>
    <div class="container section" id="warranty-block-wrap">
        <div class="row">
            <div class="col-lg-6 company-left">
                <div class="service-main-pic">
                    <div>
                        <img src="<?php bloginfo("template_url"); ?>/inc/img/service-links.png" alt="">
                        <div class="service-links-txt">
                            Наша сервисная служба одна из самых опытных – 70% заявок <br>решаются в момент обращения
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 company-right">
                <div class="service-main-block order-in-service">
                    <p>Сервисное обслуживание оборудования позволяет продлить срок его службы, гарантирует надежную,
                        непрерывную и стабильно - высокую работу предприятия. Своевременное обслуживание и проведение
                        регулярных плановых осмотров инженерами Агропак, исключит преждевременный износ составных
                        частей, значительно снизит затраты на ремонт и вынужденное время простоя производства.</p>
                    <p>Инженеры технической службы Агропак обеспечат бесперебойную работу вашего оборудования. Опыт
                        работы и знания, полученные в течение 20ти лет позволяют решить даже самую сложную
                        производственную задачу. Сотрудники службы сервиса ежегодно проходят обучение для того, чтобы
                        работать с машинами последнего поколения.</p>
                    <p class="service-last-p">Все оригинальные запасные части и комплектующие, необходимые для ремонта
                        или плановой диагностики вашего оборудования, есть в наличии на нашем складе, а в случае
                        отсутствия таковых, в кратчайшие сроки будут доставлены в Россию напрямую с
                        завода-изготовителя.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container section" id="service-order">
        <div class="row">
            <div class="col-lg-6 company-left">
                <?php echo do_shortcode('[contact-form-7 id="517" title="Заказ запчастей"]'); ?>
            </div>
            <div class="col-lg-6 company-right">
                <div class="product-model-3d__plug">
                    <div class="product-model-3d__plug_img">
                        <svg width="144" height="116" viewBox="0 0 144 116" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path d="M57.9021 68.3843L0 34.952V57.7467C0 67.8777 5.53846 76.9956 14.0979 82.0611L72 115.493V92.6987C72 83.0742 66.4615 73.4498 57.9021 68.3843Z"
                                  fill="#00AD7A"/>
                            <path d="M86.0978 33.4323L144 0V22.7948C144 32.9258 138.461 42.0437 129.902 47.1092L71.9999 81.048V58.2533C71.9999 48.1223 77.5384 38.4978 86.0978 33.4323Z"
                                  fill="#00AD7A"/>
                        </svg>
                    </div>
                    <p class="product-model-3d__plug_caption">Раздел обновляется,
                        <br>скоро здесь появится информация</p>
                    <p class="product-model-3d__plug_text">Хотите узнать подробнее,
                        <br>подпишитесь на нашу рассылку</p>
                </div>
                <!--<div class="service-order-wrap">
                    <h2>Отследить заказ <span>A024108-РФ</span></h2>
                    <div class="progressbar-wrapper">
                        <ul class="progressbar">
                            <li>Заказ создан</li>
                            <li>В обработке</li>
                            <li>В пути</li>
                            <li class="active">Доставлен</li>
                        </ul>
                    </div>
                    <div class="button-order-wrap">
                        <div class="search-order">
                            <input name="search-order" class="track-input" placeholder="Введите ваш трек номер">
                        </div>
                        <div class="button-service button-service-order">
                            <a href="#">Найти</a>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
    </div>

    <div class="container section" id="warranty-block-wrap">
        <div class="row">
            <div class="col-lg-6 company-left">
                <div class="warranty-block">
                    <div class="warranty-pic warranty-pic-service">
                        <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-warranty.png">
                    </div>
                    <div class="warranty-txt">
                        <h2 class="service-subtitle service-subtitle-1">Порядок и сроки гарантийного обслуживания</h2>
                        <div>
                            На все оборудование распространяется гарантия 12 месяцев. В течение гарантийного срока,
                            технические специалисты Агропак обеспечат бесперебойную работу установленного на предприятии
                            оборудования. Гарантийной неисправностью признаются все заводские дефекты, за исключением
                            расходных запчастей, износ которых напрямую зависит от условий эксплуатации оборудования.
                            Все гарантийные случаи имеют нависший приоритет для нашей компании и решаются в первую
                            очередь. Наша цель – обеспечить бесперебойную работу вашей упаковочной линии.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 company-right">
                <div class="warranty-block">
                    <div class="warranty-pic warranty-pic-service">
                        <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-postwarranty.png">
                    </div>
                    <div class="warranty-txt">
                        <h2 class="service-subtitle service-subtitle-2">Постгарантийное обслуживание</h2>
                        <div>
                            По истечению гарантийного срока, вы можете продлить сервисный договор на платной основе с
                            возможностью автоматической пролонгации. Перед заключением договора постгарантийного
                            обслуживания, сервисный инженер Агропак выезжает на ваше предприятие, проводит осмотр и
                            диагностику установленного оборудования для составления индивидуального договора.
                            <br>В рамках договора постгарантийного обслуживания проводится плановый ремонт, диагностика
                            оборудования и замена расходных запчастей. Запасные части есть в наличии, на нашем складе в
                            России, или же могут быть заказаны и доставлены в короткий срок с завода-изготовителя.
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <section class="container engineer-wrap">
        <div class="servicecity">
            <div style="width: 100%;">
                <h2>сервисные инженеры</h2>
            </div>
        </div>
        <?php echo do_shortcode('[metaslider id="96"]'); ?>
        <div id="flexslider2" class="flexslider">
            <ul class="slides">

                <?php
                // service block
                if (have_posts()) :
                    function num2word($num, $words)
                    {
                        $num = $num % 100;
                        if ($num > 19) {
                            $num = $num % 10;
                        }
                        switch ($num) {
                            case 1:
                            {
                                return ($words[0]);
                            }
                            case 2:
                            case 3:
                            case 4:
                            {
                                return ($words[1]);
                            }
                            default:
                            {
                                return ($words[2]);
                            }
                        }
                    }

                    query_posts('cat=30&posts_per_page=15');
                    while (have_posts()) : the_post(); ?>
                        <li>
                            <div class="service-engineers">
                                <div class="service-experience">
                                    <div class="service-img">
                                        <img src="<?php echo the_post_thumbnail_url(); ?>">
                                    </div>
                                    <div class="experience-value">
                                        <div class="experience-sign">
                                            Стаж <?php echo get_field('experience');
                                            echo num2word(get_field('experience'), array(' год', ' года', ' лет')); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="service-title"><?php the_title(); ?></div>
                            </div>
                        </li>
                    <?php
                    endwhile;
                endif;
                ?>
            </ul>
        </div>
    </section>

    <div class="container section engineering-wrap modernization-block">
        <div class="row">
            <div class="col-lg-6">
                <div class="engineering">
                    <h2>Модернизация производства</h2>
                </div>
                <p>
                    Модернизация упаковочных линий одна из опций, которая доступна нашим клиентам. Комплексное,
                    частичное или полное обновление оснащения на предприятии – потребность, которая рано или поздно
                    возникает на любом производстве.
                </p>
                <p>
                    Обновление производственных линий и ее элементов снижает затраты, длительность цикла и количество
                    производственных ошибок на 20-40%.
                </p>
                <p class="service-last-p">
                    К обновлению производственных элементов нужно подходить основательно. Инженеры Агропак произведут
                    перепроектирование вашего объекта, тестирование и диагностику оборудования, а после установят новое
                    оборудование.
                </p>
            </div>
            <div class="col-lg-6 service-pics">
                <div>
                    <img src="<?php bloginfo("template_url"); ?>/inc/img/service-pics.png">
                    <div class="service-pics-txt">
                        <div>Модернизация</div>
                        <div>Инжиниринг</div>
                        <div>Сервис</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container section engineering-wrap five-steps">
        <div class="row">
            <div class="col-lg-6">
                <div class="engineering">
                    <h2>5 шагов к модернизации производства: <br>как мы работаем?</h2>
                </div>
                <div class="work-step-wrap after-five-steps">
                    <div class="work-step-txt">
                        Инжиниринг в формате консультации со специалистом. Мы рассчитываем, насколько собственная
                        производственная база будет выгодна для компании. Подбираем тип оборудования для производства
                        упаковки, которая позволит оптимизировать затраты и при этом повысить конкурентоспособность
                        бренда на рынке. Консультируем по финансовым вопросам – рассказываем, как получить инвестиции на
                        проектное оборудование через государственные программы льготного кредитования, субсидирование и
                        лизинг.
                    </div>
                    <div class="work-step-number">
                        1
                    </div>
                </div>
                <div class="work-step-wrap">
                    <div class="work-step-txt">
                        Проектирование. Разработка индивидуальной высокотехнологичной упаковочной линии. При разработке
                        проекта создаем решения, опираясь на европейский опыт и решения, но при этом, сохраняем
                        доступную для начинающих агропредприятий цену. Предоставляем схемы, чертежи проекта, а также
                        экономические выкладки и всю необходимую документацию.
                    </div>
                    <div class="work-step-number">
                        2
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="work-step-wrap">
                    <div class="work-step-txt">
                        Поставка оборудования. Сопровождаем оборудование и отслеживаем доставку в цепочке «завод –
                        клиент». Несем ответственность за безопасную транспортировку уникальных технических решений,
                        обеспечиваем страхование и защиту груза.
                    </div>
                    <div class="work-step-number">
                        3
                    </div>
                </div>
                <div class="work-step-wrap">
                    <div class="work-step-txt">
                        Монтаж и наладка оборудования. Работаем «под ключ» - не просто разрабатываем и поставляем
                        технические решения, а доводим партнеров до результата, потому что главная наша миссия – помощь
                        производителем фруктов и овощей в России. Наши сервисные инженеры доставляют, устанавливают,
                        запускают и тестируют оборудование. Показывают технологию работы сотрудникам предприятия,
                        повторно проверяют мощность решений.
                    </div>
                    <div class="work-step-number">
                        4
                    </div>
                </div>
                <div class="work-step-wrap">
                    <div class="work-step-txt">
                        Сервисное обслуживание. Наши специалисты оказывают 100% работоспособность оборудования при
                        помощи профилактических мер, соблюдения гарантии, а в случае поломки занимаются текущим ремонтом
                        оборудования, поставкой запасных частей.
                    </div>
                    <div class="work-step-number">
                        5
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php
get_footer();
