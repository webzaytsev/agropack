    <section class="container section">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="service-h1">Выберите направление интересующего проекта</h1>
                <div class="equipment-main projects-wide">
                    <div class="equip-main-wrap">
                        <a href="/projects/otkrytyj-grunt">
                            <div class="equip-main-circle">
                                <div class="equip-main">
                                    <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-1.svg" alt="Открытый грунт">
                                </div>
                            </div>
                            <div class="equip-main-link">
                                Открытый грунт
                            </div>
                        </a>
                    </div>
                    <div class="equip-main-wrap">
                        <a href="/projects/zashhishhennyj-grunt">
                            <div class="equip-main-circle">
                                <div class="equip-main">
                                    <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-2.svg" alt="Защищенный грунт">
                                </div>
                            </div>
                            <div class="equip-main-link">
                                Защищенный грунт
                            </div>
                        </a>
                    </div>
                    <div class="equip-main-wrap">
                        <a href="/projects/sadovodstvo">
                            <div class="equip-main-circle">
                                <div class="equip-main">
                                    <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-3.svg" alt="Садоводство">
                                </div>
                            </div>
                            <div class="equip-main-link">
                                Садоводство
                            </div>
                        </a>
                    </div>
                    <div class="equip-main-wrap">
                        <a href="/projects/jagody-i-frukty">
                            <div class="equip-main-circle">
                                <div class="equip-main">
                                    <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-4.svg" alt="Ягоды и фрукты">
                                </div>
                            </div>
                            <div class="equip-main-link">
                                Ягоды и фрукты
                            </div>
                        </a>
                    </div>
                </div>

                <div class="projects-narrow">
                  <?php echo do_shortcode('[metaslider id="96"]');?>
                    <div id="flexslider-category" class="flexslider">
                        <ul class="slides">
                            <li>
                                <div class="equip-main-wrap">
                                    <a href="/projects/otkrytyj-grunt">
                                        <div class="equip-main-circle">
                                            <div class="equip-main">
                                                <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-1.svg" alt="Открытый грунт">
                                            </div>
                                        </div>
                                        <div class="equip-main-link">
                                            Открытый грунт
                                        </div>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <div class="equip-main-wrap">
                                    <a href="/projects/zashhishhennyj-grunt">
                                        <div class="equip-main-circle">
                                            <div class="equip-main">
                                                <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-2.svg" alt="Защищенный грунт">
                                            </div>
                                        </div>
                                        <div class="equip-main-link">
                                            Защищенный грунт
                                        </div>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <div class="equip-main-wrap">
                                    <a href="/projects/sadovodstvo">
                                        <div class="equip-main-circle">
                                            <div class="equip-main">
                                                <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-3.svg" alt="Садоводство">
                                            </div>
                                        </div>
                                        <div class="equip-main-link">
                                            Садоводство
                                        </div>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <div class="equip-main-wrap">
                                    <a href="/projects/jagody-i-frukty">
                                        <div class="equip-main-circle">
                                            <div class="equip-main">
                                                <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-4.svg" alt="Ягоды и фрукты">
                                            </div>
                                        </div>
                                        <div class="equip-main-link">
                                            Ягоды и фрукты
                                        </div>
                                    </a>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
    </section>