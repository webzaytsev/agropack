<?php
	/**
	 * The template for displaying product content in the single-product.php template
	 *
	 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
	 *
	 * HOWEVER, on occasion WooCommerce will need to update template files && you
	 * (the theme developer) will need to copy the new files to your theme to
	 * maintain compatibility. We try to do this as little as possible, but it does
	 * happen. When this occurs the version of the template file will be bumped and
	 * the readme will list any important changes.
	 *
	 * @see     https://docs.woocommerce.com/document/template-structure/
	 * @package WooCommerce\Templates
	 * @version 3.6.0
	 */

	defined( 'ABSPATH' ) || exit;

	global $product;

	$prod_terms = get_the_terms( $post->ID, 'product_cat' );
foreach ( $prod_terms as $prod_term ) {
	$product_cat_id                         = $prod_term->term_id;
	$product_parent_categories_all_hierachy = get_ancestors( $product_cat_id, 'product_cat' );
	$last_parent_cat                        = array_slice( $product_parent_categories_all_hierachy, -1, 1, true );
	foreach ( $last_parent_cat as $last_parent_cat_value ) {
		$top_parent = $last_parent_cat_value;
	}
}

	/**
	 * Hook: woocommerce_before_single_product.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 */
	do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
	<?php
		/**
		 * Hook: woocommerce_before_single_product_summary.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>
	<div class="summary entry-summary">
		<?php
			/**
			 * Hook: woocommerce_single_product_summary.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 * @hooked WC_Structured_Data::generate_product_data() - 60
			 */

			add_action(
				'woocommerce_single_product_summary',
				function () {
					// template for this is in storefront-child/woocommerce/single-product/product-attributes.php
					global $product;
					echo $product->list_attributes();
				},
				15
			);
			remove_action( 'woocommerce_single_product_summary', 'the_content', 20 );
			add_action( 'woocommerce_single_product_summary', 'woocommerce_excerpt_after_attribs', 20 );
			function woocommerce_excerpt_after_attribs() {
				if ( get_the_excerpt() ) {
					echo '<div class="poduct-descr-wrap">';
					the_content();
					echo '</div>';
				}
			}

			do_action( 'woocommerce_single_product_summary' );
			$analog_equip = get_field_object( 'analog_equipment' );
			if ( $analog_equip ) {
				$analog_idgoods   = $analog_equip['value'];
				$analog_permalink = get_the_permalink( $analog_idgoods );
			}
			$btn_pack_eq = 'product-order';
			$btn_name    = 'Отправить заявку';
			if ( $top_parent == 22 ) {
				$btn_pack_eq = 'packing-request';
				$btn_name    = 'Заказать упаковку';
			}
			?>
		<div class="button-product-action">
			<div class="button-product send-order <?php echo $btn_pack_eq; ?>">
				<a href="#"><?php echo $btn_name; ?></a>
			</div>
			<?php
			if ( $analog_equip && $analog_equip['value'] > 0 ) :
				?>
			<div class="button-product have-questions">
				<a href="<?php echo $analog_permalink; ?>"><span>Посмотреть<br><span>российский аналог</span></span></a>
			</div>
			<?php endif; ?>
			<?php
			if ( $top_parent == 22 ) :
				$equipment_pack = get_field_object( 'equipment_for_pack' );
				if ( $equipment_pack ) {
					$equipment_pack_id        = $equipment_pack['value'];
					$equipment_pack_permalink = get_the_permalink( $equipment_pack_id );
				}
				if ( $equipment_pack && $equipment_pack['value'] > 0 ) :
					?>
			<div class="button-product have-questions equipment-pickup">
				<a href="<?php echo $equipment_pack_permalink; ?>"><span>Подобрать<br><span>оборудование</span></span></a>
			</div>
					<?php
				endif;
				endif;
			?>
		</div>
	</div>
	<?php
		/**
		 * Hook: woocommerce_after_single_product_summary.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		// Похожие товары
		// do_action( 'woocommerce_after_single_product_summary' );
	?>
</div>
<?php
	$equip = get_field_object( 'pack_equipment' );
if ( $equip && $equip['value'] > 0 && ( $top_parent == 19 ) ) :
	?>
<div class="product-after-div">
	<div class="container">
		<h2 class="material-combination with-line">Вид упаковки</h2>
		<?php echo do_shortcode( '[metaslider id="96"]' ); ?>
		<div id="flexslider3" class="flexslider">
			<ul class="products slides">
				<?php
				global $post;
				for ( $i = 0; $i < count( $equip['value'] ); $i++ ) {
					$idgoods = $equip['value'][ $i ];
					if ( isset( $idgoods ) ) {
						$tovar_permalink = get_the_permalink( $idgoods );
						echo '<li class="product type-product status-publish has-post-thumbnail product-type-simple">
                     <a href="' . $tovar_permalink . '">
                     <div class="product-item-wrap">';
						$thumb_id  = get_post_thumbnail_id( $idgoods );
						$thumb_url = wp_get_attachment_image_src( $thumb_id, 'thumbnail-size', true );
						if ( $thumb_id != 0 ) {
							echo '<img src="' . $thumb_url[0] . '" alt="' . esc_html( get_the_title( $idgoods ) ) . '">';
						} else {
							echo '<img src="/wp-content/uploads/woocommerce-placeholder-600x600.png">';
						}
						echo '<h2 class="woocommerce-loop-product__title">' . esc_html( get_the_title( $idgoods ) ) . '</h2>';

						echo '</div></a>
                     </li>';
					}
				}
				?>
			</ul>
		</div>
	</div>
</div>
<?php endif; ?>
<?php
	// $material_1 = get_field_object("material_1");
	// if ($equip && ($material_1['value'] > 0 || $material_2['value'] > 0 || $material_3['value'] > 0 || $material_4['value'] > 0)) :
?>
<?php // add_action('woocommerce_after_main_content', 'new_div', 50); ?>
<?php
function new_div() {
	$prod_terms = get_the_terms( $post->ID, 'product_cat' );
	foreach ( $prod_terms as $prod_term ) {
		$product_cat_id                         = $prod_term->term_id;
		$product_parent_categories_all_hierachy = get_ancestors( $product_cat_id, 'product_cat' );
		$last_parent_cat                        = array_slice( $product_parent_categories_all_hierachy, -1, 1, true );
		foreach ( $last_parent_cat as $last_parent_cat_value ) {
			$top_parent = $last_parent_cat_value;
		}
	}
	global $product;
	if ( $top_parent == 19 ) :
		?>
		<?php $taxonomies = get_the_terms( $product->get_id(), 'materials' ); ?>
<div class="product-after-div <?php echo $taxonomies ? 'md-half-width' : ''; ?> ">
	<div class="product-after-info">
		<div class="row">
			<div class="col-lg-6">
				<h3>Модернизация производственных<br> и упаковочных линий</h3>
				<div class="mb-30">Внесение изменений и добавление новых комбинаций в рабочую упаковочную линию одна из опций, которая доступна нашим клиентам.
				</div>
				<div>Наша компания проведёт модернизацию установленных ранее на предприятии линий по предпродажной подготовке, калибровке и упаковке свежих овощей и фруктов. Модернизация способна значительно увеличить производительность упаковочных линий и расширить ассортимент выпускаемой продукции.
				</div>
			</div>
			<div class="col-lg-6 service-pics">
				<div>
					<img src="<?php bloginfo( 'template_url' ); ?>/inc/img/service-pics.png">
					<div class="service-pics-txt">
						<div>Модернизация</div>
						<div>Инжиниринг</div>
						<div>Сервис</div>
					</div>
				</div>
			</div>
		</div>
		<div class="service-pics">
			<div>
			</div>
		</div>
	</div>
	<section class="container section projects-page map-product">
		<h2 class="finance-header projects-header">Данное оборудование успешно работает</h2>
		<div class="map-wrap-block">
			<div class="map-equipment map-projects">
			<?php
				echo do_shortcode( '[russiahtml5map id="' . get_field( 'map_our_clients' ) . '"] ' );
			?>
			</div>
		</div>
	</section>
</div>
<?php elseif ( $top_parent == 22 ) : ?>
<div class="product-after-div individual-design-wrap md-half-width sl2">
	<div class="">
		<?php
			global $post;
			$images = acf_photo_gallery( 'iIndividual_design', $post->ID );
		if ( count( $images ) ) :
			?>
			<?php echo do_shortcode( '[metaslider id="96"]' ); ?>
		<div class="material-combination with-line nowith-line">
			Индивидуальный дизайн упаковки
			<div class="nav-flip">
				<button class="prev"><img src="https://dev.agropak.ru/wp-content/themes/agropak/inc/img/arrow-l.svg"></button>
				<button class="next"><img src="https://dev.agropak.ru/wp-content/themes/agropak/inc/img/arrow-r.svg"></button>
			</div>
		</div>
		<div class="flexslider-individual-wrap1" style="max-width: 540px;">
			<div id="flexslider-individual-design" class="flexslider">
				<ul class="slides">
				<?php foreach ( $images as $image ) : ?>
					<li>
						<a href="<?php echo $image['full_image_url']; ?>">
							<div class="photo-announce-individual colorbox galery-preview"
								style="background: url(<?php echo $image['full_image_url']; ?>) no-repeat center; background-size: cover;">
								<div class="photo-magnifier"></div>
							</div>
						</a>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<div class="">
		<div class="individual-design">
			<p>Одна из составляющих успеха - это упаковка товара. Чтобы ваш продукт заметили среди
				прочих на
				полке, попробовали именно ваши овощи или фрукты, важно выбрать правильную
				упаковку.
			</p>
			<p>Выбрав индивидуальный дизайн для вашей упаковки, вы будете отличатся от конкурентов,
				повысите
				узнаваемость торговой марки, покупатели запомнят ваш продукт. Кроме этого, упаковка
				расскажет историю вашей компании, позволит донести до покупателя информацию о
				продукте, его
				сорте и регионе созревания.
			</p>
			<p class="margin-b0">Наша команда разработает для вас индивидуальный дизайн упаковки или
				поможет
				адаптировать ваш дизайн для любыхвидов упаковочных материалов.
			</p>
		</div>
	</div>
</div>
	<?php
endif;
}
?>
<div class="flex align-items-start justify-between mt-40 flex-wrap">
	<?php
		$taxonomies = get_the_terms( $product->get_id(), 'materials' );

	if ( count( $taxonomies ) ) :
		?>
	<div class="product-after-div materials-wrap md-half-width sl1">
		<?php
		if ( $taxonomies ) {
			echo '
                            <div class="material-combination with-line nowith-line">
                            Виды расходных материалов
                            <div class="nav-flip">
                                    <button class="prev"><img src="https://dev.agropak.ru/wp-content/themes/agropak/inc/img/arrow-l.svg"></button>
                                    <button class="next"><img src="https://dev.agropak.ru/wp-content/themes/agropak/inc/img/arrow-r.svg"></button>
                                </div>
                        </div>
                        ';
		}
		?>
		<!--        -->
		<?php
		// echo do_shortcode('[metaslider id="96"]');
		?>
		<div id="flexslider-individual-design2" class="flexslider">
			<ul class="slides">
			<?php $arr2 = array_chunk( $taxonomies, 4 ); ?>
			<?php foreach ( $arr2 as $taxonomy ) : ?>
				<li class="products" style="margin-bottom: 0;">
					<?php foreach ( $taxonomy as $k => $v ) : ?>
					<div class="material-list half-width">
						<div class="product-item-wrap">
							<div class="flip-container"
								ontouchstart="this.classList.toggle('hover');">
								<div class="flipper">
									<div class="front">
										<?php if ( get_field( 'material-img', 'materials_' . $v->term_id ) ) : ?>
										<img src="<?php echo get_field( 'material-img', 'materials_' . $v->term_id )['url']; ?>"
											alt="img">
										<?php else : ?>
										<img src="/wp-content/uploads/woocommerce-placeholder-600x600.png"
											alt="img">
										<?php endif; ?>
									</div>
									<div class="back">
										<div class="tech-param">
											Технические характеристики
										</div>
										<?php echo $v->description; ?>
									</div>
								</div>
							</div>
							<h2><?php echo $v->name; ?></h2>
						</div>
					</div>
					<?php endforeach; ?>
				</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
	<?php endif ?>
	<?php new_div(); ?>
</div>
<?php do_action( 'woocommerce_after_main_content' ); ?>
