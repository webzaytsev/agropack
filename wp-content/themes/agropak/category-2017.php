<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<section id="primary" class="content-area col-sm-12 col-lg-12 photos-year">
		<div id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) :
			?>

			<header class="page-header">
<div class="page-header-wrap">
<div class="page-header-title">

				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="archive-description">', '</div>' );
				?>
</div>
<div class="">
			<?php
			global $wp;
			$arr_url       = explode( '/', home_url( $wp->request ) );
			$args          = array(
				'child_of' => 31,
				'order'    => 'DSC',
			);
			$subcategories = get_categories( $args );
			if ( $subcategories ) :
				?>
<div id="cityselect">
<select onchange="location=value" class="cityselect">
				<?php
				foreach ( $subcategories as $child ) :
					$selected = '';
					if ( $arr_url[ count( $arr_url ) - 1 ] == $child->slug ) {
						$selected = ' selected';
					}
					?>
<option value="/photoalbums/<?php echo $child->slug; ?>"<?php echo $selected; ?>><?php echo $child->slug; ?></option>
	<?php endforeach; ?>
</select>
</div>
<?php endif; ?>
</div>
</div>
			</header><!-- .page-header -->

			<?php
			while ( have_posts() ) :
				the_post();
				?>
	  <div class="photoalbum-year-current">
		<div class="photoalbum-year-img"><a href="<?php the_permalink(); ?>">
		  <img src=""><img src="<?php $alb_preview = get_field('albums_preview');
			   echo $alb_preview['sizes']['albums_preview'];?>"></a>
		</div>
		<div class="photoalbum-year-title">
		   <a href="<?php the_permalink(); ?>"><span><?php the_title(); ?></span></a>
		</div>  
	  </div>  
				<?php
			endwhile;
			?>
			<?php

		endif;
		?>
		</div><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
