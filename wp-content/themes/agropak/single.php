<?php
$post = $wp_query->post;
if ( in_category( '25' ) ) {
    include( TEMPLATEPATH.'/single-news.php' );
} else if ( in_category( '30' ) ) {
    include( TEMPLATEPATH.'/single-engineers.php' );
} else if ( in_category( '137' ) ) {
    include( TEMPLATEPATH.'/single-projects.php' );
} else if ( in_category( '31' ) || in_category( '32' ) || in_category( '33' ) || in_category( '34' ) || in_category( '50' ) || in_category( '51' ) || in_category( '138' ) ) {
    include( TEMPLATEPATH.'/single-photoalbums.php' );
} else {
    include( TEMPLATEPATH.'/single-default.php' );
}

?>