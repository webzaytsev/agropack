<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<section id="primary" class="content-area col-sm-12 col-lg-12">
		<div id="main" class="site-main" role="main">
		<?php
		if ( have_posts() ) : ?>
			<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->

		<?php
		endif;
echo do_shortcode('[metaslider id="96"]');
?>
<div id="flexslider-photoalbum" class="flexslider">
  <ul class="slides">
<?php
$cat_data = get_categories( array( 'parent' => 31, 'hide_empty' => 0, 'order' => 'DSC' ) );
if ( $cat_data ) {
  foreach ( $cat_data as $one_cat_data) : 
    $albums_cat = get_posts('category='.$one_cat_data->term_id.'&numberposts=1');
      if ($albums_cat) :
$cat_url = get_field('albums_preview',$albums_cat[0]->ID);
?>
    <li>
      <div class="photoalbum-year-wrap">
      <div class="photoalbum-year">
        <div class="photoalbum-year-img"><a href="<?php echo get_category_link( $one_cat_data->term_id ); ?>">
          <img src="<?php echo $cat_url['sizes']['albums_preview']; ?>"></a>
        </div>
        <div class="photoalbum-year-title">
           <a href="<?php echo get_category_link( $one_cat_data->term_id ); ?>"><span><?php echo $one_cat_data->cat_name; ?></span></a>
        </div>
      </div>  
      </div>  
    </li>
<?php
      endif;
  endforeach;
  }
?>
  </ul>
</div>
		</div>
	</section>

<?php
get_footer();
