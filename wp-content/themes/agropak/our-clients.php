<section id="cities-slider" class="">
    <div class="filial">
        <div class="filial-wrap">
            <h2>Наши филиалы</h2>
        </div>
        <div id="flexslider-cities" class="flexslider">
            <ul class="slides">
                <li>
                    <div class="col-xl-2 col-lg-4 col-md-4 col-sm-6">
                        <div class="filial-city mobile-margin">
                            <div class="filial-city-wrap city-piter">
                                <div class="filial-city-title">
                                    Санкт-Петербург
                                </div>
                                наб. Обводного канала,<br>
                                д. 199-201, корп. Е
                                <a href="tel:8 (812) 331-88-58" class="filial-city-phone">
                                    8 (812) 331-88-58
                                </a>
                                <a href="mailto:spb@agropak.ru">spb@agropak.ru</a>
                            </div>
                            <button type="button" class="filial-piter filial__btn">Карта</button>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="col-xl-2 col-lg-4 col-md-4 col-sm-6">
                        <div class="city-moscow">
                            <div class="filial-city mobile-margin">
                                <div class="filial-city-wrap">
                                    <div class="filial-city-title">
                                        Москва
                                    </div>
                                    ул. Бутлерова, д. 17,<br>
                                    БЦ «Нео Гео»
                                    <a href="tel: 8 (495) 775-16-83" class="filial-city-phone">
                                        8 (495) 775-16-83
                                    </a>
                                    <a href="mailto:msk@agropak.ru">msk@agropak.ru</a>
                                </div>
                                <button type="button" class="filial-moscow filial__btn">Карта</button>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="col-xl-2 col-lg-4 col-md-4 col-sm-6">
                        <div class="filial-city">
                            <div class="filial-city-wrap">
                                <div class="filial-city-title">
                                    Краснодар
                                </div>
                                ул. им. Макаренко, <br>
                                д. 11, корп. 1
                                <a href="tel:8 (861) 204-19-18" class="filial-city-phone">
                                    8 (861) 204-19-18
                                </a>
                                <a href="mailto:krnd@agropak.ru">krnd@agropak.ru</a>
                            </div>
                            <button type="button" class="filial-krasnodar filial__btn">Карта</button>
                        </div>
                    </div>
                </li>
                
            </ul>
        </div>
    </div>
</section>


<div class="container filial-content">
    <div class="filial">
        <div class="row">
            <div class="col-lg">
                <div class="filial-wrap">
                    <h2>Наши филиалы</h2>
                </div>
            </div>
        </div>
        <div class="row row_custom">
            <div class="col-xl-2 col-lg-4 col-md-4 col-sm-6">
                <div class="filial-city mobile-margin">
                    <div class="filial-city-wrap city-piter">
                        <div class="filial-city-title">
                            Санкт-Петербург
                        </div>
                        наб. Обводного канала,<br>
                        д. 199-201, корп. Е
                        <a href="tel:8 (812) 331-88-58" class="filial-city-phone">
                            8 (812) 331-88-58
                        </a>
                        <a href="mailto:spb@agropak.ru">spb@agropak.ru</a>
                    </div>
                    <button type="button" class="filial-piter filial__btn">Карта</button>
                </div>
            </div>
            <div class="col-xl-2 col-lg-4 col-md-4 col-sm-6">
                <div class="city-moscow">
                <div class="filial-city mobile-margin">
                    <div class="filial-city-wrap">
                        <div class="filial-city-title">
                            Москва
                        </div>
                        ул. Бутлерова, д. 17,<br>
                        БЦ «Нео Гео»
                        <a href="tel:8 (495) 775-16-83" class="filial-city-phone">
                            8 (495) 775-16-83
                        </a>
                        <a href="mailto:msk@agropak.ru">msk@agropak.ru</a>
                    </div>
                    <button type="button" class="filial-moscow filial__btn">Карта</button>
                </div>
                </div>
            </div>
            <div class="col-xl-2 col-lg-4 col-md-4 col-sm-6">
                <div class="filial-city">
                    <div class="filial-city-wrap">
                        <div class="filial-city-title">
                            Краснодар
                        </div>
                        ул. им. Макаренко, <br>
                        д. 11, корп. 1
                        <a href="tel:8 (861) 204-19-18" class="filial-city-phone">
                            8 (861) 204-19-18
                        </a>
                        <a href="mailto:krnd@agropak.ru">krnd@agropak.ru</a>
                    </div>
                    <button type="button" class="filial-krasnodar filial__btn">Карта</button>
                </div>
            </div>
            
        </div>
    </div>
</div>