<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Agropak
 */

get_header(); ?>
	<section id="primary" class="content-area col-sm-12 col-lg-12">
		<div id="main" class="site-main" role="main">

<?php
while ( have_posts() ) :
	the_post();
	?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
	<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header>
</article>

<div class="photos-album-wrap">
	<?php
	$images = acf_photo_gallery( 'photos', $post->ID );
	if ( count( $images ) ) :
		?>
				<?php foreach ( $images as $image ) : ?>
								<a href="<?php echo $image['full_image_url']; ?>">
<div class="photo-announce colorbox galery-preview" style="background: url(<?php echo $image['full_image_url']; ?>) no-repeat center; background-size: cover;">
<div class="photo-magnifier"></div>
</div>
								</a>
							<?php endforeach; ?>
				<?php endif; ?> 
</div>

				<?php
		endwhile; // End of the loop.
?>

		</div><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
