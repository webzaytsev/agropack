jQuery(document).ready(function($){

  $(".navbar-toggler").click(function(){
    $('.burger_popup').addClass('active');
  });

  $(".burger_popup .close").click(function(){
    $('.burger_popup').removeClass('active');
  });

  $('.close_popup').on('click', function() {
    $('.popup').removeClass('active');
  });

  $("<div class='before-map'>Выберите точку на карте и узнаете о реализованном нами проекте</div>").insertBefore('.clients-top.clients-main-page #russia-html5-map-state-info_0');
  $(".clients-top.clients-main-page #russia-html5-map-state-info_0").append('<img src="/wp-content/themes/agropak/inc/img/clients/irrico.jpg"><div class="map-client-info">Группа компаний Иррико<br>Год реализации: 2018<br>Ставропольский край</div><div class="all-projects"><a href=\'/projects/\'>Изучить все проекты</a></div>');
  $("<div class='before-map'>Выберите точку на карте и узнайте больше о данном проекте.</div>").insertBefore('.clients-top.all-rojects-page #russia-html5-map-state-info_0');
  $(".clients-top.all-rojects-page #russia-html5-map-state-info_0").append('<img src="/wp-content/themes/agropak/inc/img/clients/irrico.jpg"><div class="map-client-info">Группа компаний Иррико<br>Год реализации: 2018<br>Ставропольский край</div><div class="all-projects"><a href=\'/projects/otkrytyj-grunt/gruppa-kompanij-irriko/\'>Изучить проект</a></div>');

  if ($(".flip-slider-wrap").length){
    $(window).load(function(){
      setInterval(function() {
        flip1.prevFlip();
      }, 6000);
    });
  }

  $(window).load(function() {
    $('#flexslider-category').flexslider({
      animation: "slide",
slideshow: false,
      controlNav: true,
      directionNav: true,
controlsContainer: ".flex-container",
      itemWidth: 0,
      itemMargin: 0
    });
  });
  $(window).load(function() {
    Fancybox.bind('a[href*=".jpg"], a[href*=".jpeg"], a[href*=".png"], a[href*=".gif"]', {
      
    });
  });
  $(window).load(function() {
    $('#flexslider-projects').flexslider({
      animation: "slide",
      controlNav: true,
      directionNav: true,
      minItems: 1,
      maxItems: 1
    });
  });
  $(window).load(function() {
    $('#flexslider-cities').flexslider({
      animation: "slide",
      controlNav: true,
      directionNav: false,
      minItems: 1,
      maxItems: 1
    });
  });
  $(window).load(function() {
    $('#flexslider-main').flexslider({
      animation: "slide",
      controlNav: true,
      directionNav: false,
      minItems: 1,
      maxItems: 1
    });
  });
  $(window).load(function() {
    $('#flexslider1').flexslider({
      animation: "slide",
      directionNav: false,
      animationLoop: true
    });
  });
  $(window).load(function() {
    $('#flexslider2').flexslider({
      animation: "slide",
      itemWidth: 270,
      itemMargin: 60,
      controlNav: false,
      directionNav: true
    });
  });
  $(window).load(function() {
    $('#flexslider3').flexslider({
      animation: "slide",
      itemWidth: 270,
      itemMargin: 60,
      controlNav: false,
      directionNav: true
    });
  });
  $(window).load(function() {
    $('#flexslider-combinations').flexslider({
      slideshow: false,
      animation: "slide",
      maxItems: 1,
      controlNav: false,
      directionNav: true
    });
  });

(function() {
  var $window = $(window),
      flexslider = { vars:{} };
  function getGridSize() {
    return (window.innerWidth < 380) ? 1 :
           (window.innerWidth < 992) ? 2 : 2;
  }
  $window.load(function() {
    $('#metaslider_103').flexslider({
      slideshow: false,
      animation: "slide",
      itemWidth: 360,
      itemMargin: 30,
      controlNav: false,
      directionNav: true,
      slideshowSpeed: 12000,
      minItems: getGridSize(),
      maxItems: getGridSize()
    });
  });
  $window.load(function() {
    $('#flexslider-partners').flexslider({
      slideshow: false,
      animation: "slide",
      itemWidth: 360,
      itemMargin: 30,
      controlNav: false,
      directionNav: true,
      slideshowSpeed: 12000,
      minItems: getGridSize(),
      maxItems: getGridSize()
    });
  });
  $window.load(function() {
    $('#flexslider-clients').flexslider({
      slideshow: false,
      animation: "slide",
      itemWidth: 360,
      itemMargin: 30,
      controlNav: false,
      directionNav: true,
      slideshowSpeed: 12000,
      minItems: getGridSize(),
      maxItems: getGridSize()
    });
  });
  $window.resize(function() {
    var gridSize = getGridSize();
    flexslider.vars.minItems = gridSize;
    flexslider.vars.maxItems = gridSize;
  });
}());

  $(window).load(function() {
    $('#flexslider-photoalbum').flexslider({
      animation: "slide",
      itemWidth: 396,
      itemMargin: 30,
      controlNav: false,
      directionNav: true,
      slideshowSpeed: 12000,
    });
  });
  $(window).load(function() {
    $('#flexslider-video').flexslider({
      animation: "slide",
      itemWidth: 396,
      itemMargin: 30,
      controlNav: false,
      directionNav: true,
      slideshowSpeed: 12000,
    });
  });

  $(window).load(function() {
    $('#flexslider-individual-design').flexslider({
      slideshow: false,
      animation: "slide",
      itemWidth: 250,
      itemMargin: 25,
      controlNav: false,
      directionNav: true
    });
  });

  $(window).load(function() {
    $('#flexslider-individual-design2').flexslider({
      slideshow: false,
      animation: "slide",
      
      controlNav: false,
      directionNav: true
    });
  });

  $(window).load(function() {
    $('#flexslider8').flexslider({
      animation: "slide",
      directionNav: true,
      controlNav: false,
    });
  });

  $('.order-steps > li').click(function (event) {
      $(this).children("div").slideToggle();
      event.stopPropagation();
  });

  $(".fa-search").click(function(){
    $(".moving-search, .moving-input").toggleClass("active");
    $(".moving-search input[type='text']").focus();
    $(".topphone-wrap").toggleClass("active");
    $('.search_bl').toggleClass("active");
  });

  $('.container_padding svg').on('click', function() {
    $('.search_bl').removeClass("active");
    $('.search_bl input').removeClass('active');
  });

  $('div.main-slider-wrap').show();
  $('div.equipment-wrap').show();

  $(function() {
  (function quantityProducts() {
    var $quantityArrowMinus0 = $(".quantity-arrow-minus0");
    var $quantityArrowPlus0 = $(".quantity-arrow-plus0");
    var $quantityNum0 = $(".quantity-num0");
 
    $quantityArrowMinus0.click(quantityMinus0);
    $quantityArrowPlus0.click(quantityPlus0);
 
    function quantityMinus0(event) {
      event.preventDefault();
      if ($quantityNum0.val() > 0) {
        $quantityNum0.val(+$quantityNum0.val() - 1);
      }
    }
    function quantityPlus0(event) {
      event.preventDefault();
      $quantityNum0.val(+$quantityNum0.val() + 1);
    }
  })();
  }); 

  $(function() {
  (function quantityProducts() {
    var $quantityArrowMinus = $(".quantity-arrow-minus");
    var $quantityArrowPlus = $(".quantity-arrow-plus");
    var $quantityNum = $(".quantity-num");
 
    $quantityArrowMinus.click(quantityMinus);
    $quantityArrowPlus.click(quantityPlus);
 
    function quantityMinus(event) {
      event.preventDefault();
      if ($quantityNum.val() > 0) {
        $quantityNum.val(+$quantityNum.val() - 1);
      }
    }
    function quantityPlus(event) {
      event.preventDefault();
      $quantityNum.val(+$quantityNum.val() + 1);
    }
  })();
  }); 

  $(function() {
  (function quantityProducts() {
    var $quantityArrowMinus2 = $(".quantity-arrow-minus2");
    var $quantityArrowPlus2 = $(".quantity-arrow-plus2");
    var $quantityNum2 = $(".quantity-num2");
 
    $quantityArrowMinus2.click(quantityMinus2);
    $quantityArrowPlus2.click(quantityPlus2);
 
    function quantityMinus2(event) {
      event.preventDefault();
      if ($quantityNum2.val() > 0) {
        $quantityNum2.val(+$quantityNum2.val() - 1);
      }
    }
    function quantityPlus2(event) {
      event.preventDefault();
      $quantityNum2.val(+$quantityNum2.val() + 1);
    }
  })();
  }); 

  $(function() {
  (function quantityProducts() {
    var $quantityArrowMinus3 = $(".quantity-arrow-minus3");
    var $quantityArrowPlus3 = $(".quantity-arrow-plus3");
    var $quantityNum3 = $(".quantity-num3");
 
    $quantityArrowMinus3.click(quantityMinus3);
    $quantityArrowPlus3.click(quantityPlus3);
 
    function quantityMinus3(event) {
      event.preventDefault();
      if ($quantityNum3.val() > 0) {
        $quantityNum3.val(+$quantityNum3.val() - 1);
      }
    }
    function quantityPlus3(event) {
      event.preventDefault();
      $quantityNum3.val(+$quantityNum3.val() + 1);
    }
  })();
  }); 

  $(function() {
  (function quantityProducts() {
    var $quantityArrowMinus4 = $(".quantity-arrow-minus4");
    var $quantityArrowPlus4 = $(".quantity-arrow-plus4");
    var $quantityNum4 = $(".quantity-num4");
 
    $quantityArrowMinus4.click(quantityMinus4);
    $quantityArrowPlus4.click(quantityPlus4);
 
    function quantityMinus4(event) {
      event.preventDefault();
      if ($quantityNum4.val() > 0) {
        $quantityNum4.val(+$quantityNum4.val() - 1);
      }
    }
    function quantityPlus4(event) {
      event.preventDefault();
      $quantityNum4.val(+$quantityNum4.val() + 1);
    }
  })();
  }); 

  $(function() {
  (function quantityProducts() {
    var $quantityArrowMinus5 = $(".quantity-arrow-minus5");
    var $quantityArrowPlus5 = $(".quantity-arrow-plus5");
    var $quantityNum5 = $(".quantity-num5");
 
    $quantityArrowMinus5.click(quantityMinus5);
    $quantityArrowPlus5.click(quantityPlus5);
 
    function quantityMinus5(event) {
      event.preventDefault();
      if ($quantityNum5.val() > 0) {
        $quantityNum5.val(+$quantityNum5.val() - 1);
      }
    }
    function quantityPlus5(event) {
      event.preventDefault();
      $quantityNum5.val(+$quantityNum5.val() + 1);
    }
  })();
  }); 

  var input_cnt = 1;
  $('#spare-additional-show').click(function() {
    $("#service-input-3").css("display", "block");
    input_cnt++;
    if (input_cnt > 2)  {
    $("#service-input-4").css("display", "block");
    }
    if (input_cnt > 3)  {
    $("#service-input-5").css("display", "block");
    $("#spare-additional-show").css("display", "none");
    }
  });

  $('.order-minus-3').click(function() {
    $("#service-input-3").css("display", "none");
    input_cnt--;
    if (input_cnt == 1)  {
      $("#spare-additional-show").css("display", "block");
    }
  });
  $('.order-minus-4').click(function() {
    $("#service-input-4").css("display", "none");
    input_cnt--;
    if (input_cnt == 1)  {
      $("#spare-additional-show").css("display", "block");
    }  });
  $('.order-minus-5').click(function() {
    $("#service-input-5").css("display", "none");
    input_cnt--;
    if (input_cnt == 1)  {
      $("#spare-additional-show").css("display", "block");
    }
  });


  $('#spare-code-0').keyup(function() {
    $('#spare-code-num-0').val($(this).val());
  });
  $('#spare-code-1').keyup(function() {
    $('#spare-code-num-1').val($(this).val());
  });
  $('#spare-code-2').keyup(function() {
    $('#spare-code-num-2').val($(this).val());
  });
  $('#spare-code-3').keyup(function() {
    $('#spare-code-num-3').val($(this).val());
  });
  $('#spare-code-4').keyup(function() {
    $('#spare-code-num-4').val($(this).val());
  });
  $('#spare-code-5').keyup(function() {
    $('#spare-code-num-5').val($(this).val());
  });
  $('#spare-title-1').keyup(function() {
    $('#spare-title-num-1').val($(this).val());
  });
  //$('#quantity-num-1').val($('#quantity-1').val());
  $("#spare-button").click(function(){
    $('#quantity-num-0').val($('#quantity-0').val());
    $('#quantity-num-1').val($('#quantity-1').val());
    $('#quantity-num-2').val($('#quantity-2').val());
    $('#quantity-num-3').val($('#quantity-3').val());
    $('#quantity-num-4').val($('#quantity-4').val());
    $('#quantity-num-5').val($('#quantity-5').val());
  });

  $('img').bind('contextmenu', function(e) {
    return false;
  });


  if ($(".tribe-events").length || $(".tribe-events-pg-template").length){
    $('.tribe-event-date-start').html($('.tribe-event-date-start').html().replace("@"," "));
    $('.tribe-event-date-end').html($('.tribe-event-date-end').html().replace("@"," "));
    $('.tribe-events-abbr.tribe-events-start-datetime.updated').html($('.tribe-events-abbr.tribe-events-start-datetime.updated').html().replace("@"," "));
    $('.tribe-events-abbr.tribe-events-end-datetime.dtend').html($('.tribe-events-abbr.tribe-events-end-datetime.dtend').html().replace("@"," "));
    $('.tribe-events-calendar-list__event-datetime-wrapper.tribe-common-b2').html($('.tribe-events-calendar-list__event-datetime-wrapper.tribe-common-b2').html().replace("@"," "));
    $('.tribe-events-calendar-month__calendar-event-tooltip-datetime').html($('.tribe-events-calendar-month__calendar-event-tooltip-datetime').html().replace("@"," "));
  }

  $('.sl2 button.next').on('click', function() {
    $('.sl2 .flex-next').click();
  });

  $('.sl2 button.prev').on('click', function() {
    $('.sl2 .flex-prev').click();
  });

  $('.sl1 button.next').on('click', function() {
    $('.sl1 .flex-next').click();
  });

  $('.sl1 button.prev').on('click', function() {
    $('.sl1 .flex-prev').click();
  });

  $(document).ready(function(){
        $('.tel').mask('+7 (000) 000-00-00');
    });

  $('.order-service-form').on('click', function() {
    $('.popup_services').addClass('active');
  });

  $('.feedback-form').on('click', function() {
    $('.popup_feedback').addClass('active');
  });

  $('.product-order').on('click', function() {
    $('.popup_product-order').addClass('active');
  });

  $('.packing-request').on('click', function() {
    $('.popup_packing-request').addClass('active');
  });

  $('.proizvodstvo-form').on('click', function() {
    $('.popup_proizvodstvo-form').addClass('active');
  });

  $('.finance-form').on('click', function() {
    $('.popup_finance-form').addClass('active');
  });

  $('.spare-part-order').on('click', function() {
    $('.popup_spare-part-order').addClass('active');
  });

  $('.filial-moscow').on('click', function() {
    $('.popup_filial-moscow').addClass('active');
  });

  $('.filial-piter').on('click', function() {
    $('.popup_filial-piter').addClass('active');
  });

  $('.filial-krasnodar').on('click', function() {
    $('.popup_filial-krasnodar').addClass('active');
  });

  $('.popup').on('click', function(e) {
    if (e.target !== this) {
      return;
    } else {
      $('.popup').removeClass('active');
    }
  });

});