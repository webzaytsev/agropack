=== Agropak ===

Tags: blog, custom-menu, featured-images, threaded-comments, translation-ready, right-sidebar, custom-background, e-commerce, theme-options, sticky-post, full-width-template
Requires at least: 4.0
Tested up to: 4.7

== Description ==

Agropak WordPress theme based on Bootstrap.

Page templates
* Right-sidebar (default page template)
* Left-Sidebar
* Full-Width
* Blank with container
* Blank without container Page

Other features:
* Currently using Bootstrap v4.3.1
* Font Awesome integrated
* Widgetized footer area
* WooCommerce ready
* Compatible with Contact Form 7
* Compatible with Visual Composer
* Compatible with Elementor Page Builder


This theme will be an active project which we will update from time to time. Check this page regularly for the updates.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.
