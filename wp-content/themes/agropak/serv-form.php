
        <div class="container section" id="service-order">
            <div class="row">
                <div class="col-lg-6 company-left">
<form class="order-spare">
                    <div class="service-order-wrap">
<h2>Заказ запчастей</h2>
        		<div class="container container-padding0">
            		    <div class="row">
                		<div class="col-12">
<input name="spare-title" placeholder="Введите название запчасти">
                		</div>
            		    </div>

            		    <div class="row">
                		<div class="col-lg-6">
<input name="spare-code" placeholder="Введите код запчасти">
                		</div>
                		<div class="col-lg-6 spare-padding">
                		    <div class="spare-anount-wrap">
                		        <div class="spare-anount-txt">
Количество
                		        </div>
                		        <div class="spare-anount-value">

<div class="quantity-block">
  <button class="quantity-arrow-minus"> <i class="fas fa-caret-left"></i> </button>
  <input class="quantity-num" name="quantity1" type="number" value="0" />
  <button class="quantity-arrow-plus"> <i class="fas fa-caret-right"></i> </button>
</div>
                		        </div>
                		    </div>
                		</div>
                	    </div>
            		    <div class="row">
                		<div class="col-lg-6">
<input name="spare-code2" placeholder="Введите код запчасти">
                		</div>
                		<div class="col-lg-6 spare-padding">
                		    <div class="spare-anount-wrap">
                		        <div class="spare-anount-txt">
Количество
                		        </div>
                		        <div class="spare-anount-value">

<div class="quantity-block">
  <button class="quantity-arrow-minus2"> <i class="fas fa-caret-left"></i> </button>
  <input class="quantity-num2" name="quantity2" type="number" value="0" />
  <button class="quantity-arrow-plus2"> <i class="fas fa-caret-right"></i> </button>
</div>
                		        </div>
                		    </div>
                		</div>
                	    </div>

            		    <div class="row">
                		<div class="col-lg-6">
Добавить еще запчасти
                		</div>
                		<div class="col-lg-6">
<div class="button-service-wrap">
<div class="button-service">
  <a href="#">Заказать</a>
</div>
</div>
                		</div>
                	    </div>

       			</div>

                    </div>
</form>
                </div>
                <div class="col-lg-6 company-right">
                    <div class="service-order-wrap">
<h2>Отследить заказ <span>A024108-РФ</span></h2>
                    </div>
		</div>
            </div>
        </div>