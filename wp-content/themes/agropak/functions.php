<?php
/**
 * Agropak functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package agropak
 */

if ( ! function_exists( 'agropak_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function agropak_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Agropak, use a find and replace
	 * to change 'agropak' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'agropak', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'agropak' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'agropak_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

}
endif;
add_action( 'after_setup_theme', 'agropak_setup' );


/**
 * Add Welcome message to dashboard
 */
function agropak_reminder(){
        $theme_page_url = 'https://afterimagedesigns.com/agropak/?dashboard=1';

            if(!get_option( 'triggered_welcomet')){
                $message = sprintf(__( 'Welcome to Agropak Theme! Before diving in to your new theme, please visit the <a style="color: #fff; font-weight: bold;" href="%1$s" target="_blank">theme\'s</a> page for access to dozens of tips and in-depth tutorials.', 'agropak' ),
                    esc_url( $theme_page_url )
                );

                printf(
                    '<div class="notice is-dismissible" style="background-color: #6C2EB9; color: #fff; border-left: none;">
                        <p>%1$s</p>
                    </div>',
                    $message
                );
                add_option( 'triggered_welcomet', '1', '', 'yes' );
            }

}
add_action( 'admin_notices', 'agropak_reminder' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function agropak_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'agropak_content_width', 1170 );
}
add_action( 'after_setup_theme', 'agropak_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function agropak_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'agropak' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'agropak' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer 1', 'agropak' ),
        'id'            => 'footer-1',
        'description'   => esc_html__( 'Add widgets here.', 'agropak' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer 2', 'agropak' ),
        'id'            => 'footer-2',
        'description'   => esc_html__( 'Add widgets here.', 'agropak' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer 3', 'agropak' ),
        'id'            => 'footer-3',
        'description'   => esc_html__( 'Add widgets here.', 'agropak' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
}
add_action( 'widgets_init', 'agropak_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function agropak_scripts() {
	// load bootstrap css
    if ( get_theme_mod( 'cdn_assets_setting' ) === 'yes' ) {
        wp_enqueue_style( 'agropak-bootstrap-css', 'https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css' );
        wp_enqueue_style( 'agropak-fontawesome-cdn', 'https://use.fontawesome.com/releases/v5.15.1/css/all.css' );
    } else {
        wp_enqueue_style( 'agropak-bootstrap-css', get_template_directory_uri() . '/inc/assets/css/bootstrap.min.css' );
        wp_enqueue_style( 'agropak-fontawesome-cdn', get_template_directory_uri() . '/inc/assets/css/fontawesome.min.css' );
    }

	// load bootstrap css
	// load AItheme styles
	// load Agropak styles
	wp_enqueue_style( 'agropak-style', get_stylesheet_uri() );
    if(get_theme_mod( 'theme_option_setting' ) && get_theme_mod( 'theme_option_setting' ) !== 'default') {
        wp_enqueue_style( 'agropak-'.get_theme_mod( 'theme_option_setting' ), get_template_directory_uri() . '/inc/assets/css/presets/theme-option/'.get_theme_mod( 'theme_option_setting' ).'.css', false, '' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'poppins-lora') {
        wp_enqueue_style( 'agropak-poppins-lora-font', 'https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i|Poppins:300,400,500,600,700' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'montserrat-merriweather') {
        wp_enqueue_style( 'agropak-montserrat-merriweather-font', 'https://fonts.googleapis.com/css?family=Merriweather:300,400,400i,700,900|Montserrat:300,400,400i,500,700,800' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'poppins-poppins') {
        wp_enqueue_style( 'agropak-poppins-font', 'https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'roboto-roboto') {
        wp_enqueue_style( 'agropak-roboto-font', 'https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'arbutusslab-opensans') {
        wp_enqueue_style( 'agropak-arbutusslab-opensans-font', 'https://fonts.googleapis.com/css?family=Arbutus+Slab|Open+Sans:300,300i,400,400i,600,600i,700,800' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'oswald-muli') {
        wp_enqueue_style( 'agropak-oswald-muli-font', 'https://fonts.googleapis.com/css?family=Muli:300,400,600,700,800|Oswald:300,400,500,600,700' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'montserrat-opensans') {
        wp_enqueue_style( 'agropak-montserrat-opensans-font', 'https://fonts.googleapis.com/css?family=Montserrat|Open+Sans:300,300i,400,400i,600,600i,700,800' );
    }
    if(get_theme_mod( 'preset_style_setting' ) === 'robotoslab-roboto') {
        wp_enqueue_style( 'agropak-robotoslab-roboto', 'https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700|Roboto:300,300i,400,400i,500,700,700i' );
    }
    if(get_theme_mod( 'preset_style_setting' ) && get_theme_mod( 'preset_style_setting' ) !== 'default') {
        wp_enqueue_style( 'agropak-'.get_theme_mod( 'preset_style_setting' ), get_template_directory_uri() . '/inc/assets/css/presets/typography/'.get_theme_mod( 'preset_style_setting' ).'.css', false, '' );
    }

    wp_enqueue_style( 'agropak-cardslider', '/wp-content/themes/agropak/style1.css' );
    wp_enqueue_style( 'fancybox', 'https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css' );

    //wp_enqueue_style( 'agropak-cardslider', '/wp-content/themes/agropak/inc/assets/css/cardslider.css' );
    //wp_enqueue_style( 'agropak-events', '/wp-content/themes/agropak/inc/assets/css/events.css' );


    //Color Scheme
    /*if(get_theme_mod( 'preset_color_scheme_setting' ) && get_theme_mod( 'preset_color_scheme_setting' ) !== 'default') {
        wp_enqueue_style( 'agropak-'.get_theme_mod( 'preset_color_scheme_setting' ), get_template_directory_uri() . '/inc/assets/css/presets/color-scheme/'.get_theme_mod( 'preset_color_scheme_setting' ).'.css', false, '' );
    }else {
        wp_enqueue_style( 'agropak-default', get_template_directory_uri() . '/inc/assets/css/presets/color-scheme/blue.css', false, '' );
    }*/


	wp_enqueue_script('jquery');

    // Internet Explorer HTML5 support
    wp_enqueue_script( 'html5hiv',get_template_directory_uri().'/inc/assets/js/html5.js', array(), '3.7.0', false );
    wp_script_add_data( 'html5hiv', 'conditional', 'lt IE 9' );

	// load bootstrap js
    if ( get_theme_mod( 'cdn_assets_setting' ) === 'yes' ) {
        wp_enqueue_script('agropak-popper', 'https://cdn.jsdelivr.net/npm/popper.js@1/dist/umd/popper.min.js', array(), '', true );
    	wp_enqueue_script('agropak-bootstrapjs', 'https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js', array(), '', true );
    } else {
        wp_enqueue_script('agropak-popper', get_template_directory_uri() . '/inc/assets/js/popper.min.js', array(), '', true );
        wp_enqueue_script('agropak-bootstrapjs', get_template_directory_uri() . '/inc/assets/js/bootstrap.min.js', array(), '', true );
    }
    wp_enqueue_script('agropak-themejs', get_template_directory_uri() . '/inc/assets/js/theme-script.min.js', array(), '', true );
	wp_enqueue_script( 'agropak-skip-link-focus-fix', get_template_directory_uri() . '/inc/assets/js/skip-link-focus-fix.min.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

    wp_enqueue_script( 'fancybox', 'https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js', array('jquery') );
    wp_enqueue_script( 'customjs', get_template_directory_uri() . '/inc/custom.js', array('fancybox') );
    //wp_enqueue_script( 'customjs', get_template_directory_uri() . '/inc/custom.js', array('jquery'), null, true );

    //wp_enqueue_script( 'flexslider-2', get_template_directory_uri() . '/inc/assets/js/jquery.flexslider.min.js', array( 'jquery' ), '', true );
    //wp_enqueue_style( 'flexslider-2-css', get_template_directory_uri() . '/inc/assets/css/flexslider.css' );

    if ( is_home() ) {
	    wp_enqueue_script('agropak-flipslider', get_template_directory_uri() . '/inc/assets/js/flipslide.js', array(), '', true );
    }

    
    wp_enqueue_script('agropak-phonemask', get_template_directory_uri() . '/inc/assets/js/jquery.mask.js', array(), '', true );

}
add_action( 'wp_enqueue_scripts', 'agropak_scripts' );

/**
 * Add Preload for CDN scripts and stylesheet
 */
function agropak_preload( $hints, $relation_type ){
    if ( 'preconnect' === $relation_type && get_theme_mod( 'cdn_assets_setting' ) === 'yes' ) {
        $hints[] = [
            'href'        => 'https://cdn.jsdelivr.net/',
            'crossorigin' => 'anonymous',
        ];
        $hints[] = [
            'href'        => 'https://use.fontawesome.com/',
            'crossorigin' => 'anonymous',
        ];
    }
    return $hints;
} 

add_filter( 'wp_resource_hints', 'agropak_preload', 10, 2 );



function agropak_password_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<form action="' . esc_url( home_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post">
    <div class="d-block mb-3">' . __( "To view this protected post, enter the password below:", "agropak" ) . '</div>
    <div class="form-group form-inline"><label for="' . $label . '" class="mr-2">' . __( "Password:", "agropak" ) . ' </label><input name="post_password" id="' . $label . '" type="password" size="20" maxlength="20" class="form-control mr-2" /> <input type="submit" name="Submit" value="' . esc_attr__( "Submit", "agropak" ) . '" class="btn btn-primary"/></div>
    </form>';
    return $o;
}
add_filter( 'the_password_form', 'agropak_password_form' );



/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load plugin compatibility file.
 */
require get_template_directory() . '/inc/plugin-compatibility/plugin-compatibility.php';

/**
 * Load custom WordPress nav walker.
 */
if ( ! class_exists( 'wp_bootstrap_navwalker' )) {
    require_once(get_template_directory() . '/inc/wp_bootstrap_navwalker.php');
}
add_filter( 'woocommerce_product_tabs', 'sb_woo_remove_reviews_tab', 98);
function sb_woo_remove_reviews_tab($tabs) {
unset($tabs['reviews']);
return $tabs;
}
add_filter('wpseo_breadcrumb_single_link' ,'hotel_remove_shop', 10 ,2);
function hotel_remove_shop($link_output, $link ){
if( $link['text'] == 'Магазин') {
$link_output = '';
}
return $link_output;
}

//preview news
function new_excerpt_length($length) {
    return 12;
}
add_filter('excerpt_length', 'new_excerpt_length');
function new_excerpt_more($more) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

//убрать сортировку товаров
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

// Удалить краткое описание товара
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
// На место краткого описания добавить описание товара
add_action( 'woocommerce_single_product_summary', 'the_content', 20 );
// Удалить вкладку с описанием товара
add_filter( 'woocommerce_product_tabs', 'remove_woocommerce_product_tabs', 100 );
function remove_woocommerce_product_tabs( $tabs ) {
    unset( $tabs['description'] );              // удалить вкладку описание
    unset( $tabs['additional_information'] );      // удалить вкладку свойств
    unset( $tabs['reviews'] );                     // удалить вкладку отзывов
    return $tabs;
}
// Убрать надпись Показаны товары
remove_action ( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
//убрать категорию из карточки товара
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
//убрать для рубрик заголовок Рубрика:
add_filter( 'get_the_archive_title', 'artabr_remove_name_cat' );
function artabr_remove_name_cat( $title ){
	if ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$title = single_tag_title( '', false );
	}
	return $title;
}
// пресет фото для обложки галереи
add_image_size( 'albums_preview', 600, 400, array( 'center', 'center' ) );

add_action( 'wp_enqueue_scripts', 'enqueue_colorbox' );
function enqueue_colorbox() {
$request_url = $_SERVER['REQUEST_URI'];
$search_url = "equipment";
$search_url2 = "upakovka";
//    if(!is_front_page() && !strpos($request_url, $search_url) && !strpos($request_url, $search_url2)){ 
    if(!is_front_page() && !strpos($request_url, $search_url)){ 
 wp_enqueue_style( 'colorbox-css', get_template_directory_uri(). '/colorbox/colorbox.css' );
 wp_enqueue_script( 'colorbox',get_template_directory_uri() . '/colorbox/jquery.colorbox-min.js', array( 'jquery' ), '', true );
 wp_enqueue_script( 'colorbox-init', get_template_directory_uri() . '/colorbox/colorbox-init.js', array( 'colorbox' ), '', true ); 
 wp_enqueue_script( 'colorbox-ru', get_template_directory_uri() . '/colorbox/i18n/jquery.colorbox-ru.js', array( 'colorbox' ), '', true ); 
    }
}

add_filter('wpcf7_autop_or_not', '__return_false');

add_filter( 'woocommerce_page_title', 'customize_woocommerce_page_title', 10, 1 );
function customize_woocommerce_page_title( $page_title) {
    if ( is_product_category('gardening') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для садоводства</span>';
    }
    if ( is_product_category('openground') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для открытого грунта</span>';
    }
    if ( is_product_category('protectedground') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для защищенного грунта</span>';
    }
    if ( is_product_category('berries-fruits') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для ягод и фруктов</span>';
    }
    elseif ( is_product_category('uborka') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для уборки</span>';
    }
    elseif ( is_product_category('obrabotka-gardening') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для обработки</span>';
    }
    elseif ( is_product_category('calibration') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для калибровки</span>';
    }
    elseif ( is_product_category('vzveshivanie-gardening') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для взвешивания</span>';
    }
    elseif ( is_product_category('upakovka-gardening') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для упаковки</span>';
    }
    elseif ( is_product_category('podgotovka') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для подготовки</span>';
    }
    elseif ( is_product_category('kalibrovka-openground') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для калибровки</span>';
    }
    elseif ( is_product_category('vzveshivanie-openground') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для взвешивания</span>';
    }
    elseif ( is_product_category('obrabotka-openground') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для обработки</span>';
    }
    elseif ( is_product_category('upakovka-openground') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для упаковки</span>';
    }
    elseif ( is_product_category('logistika') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для логистики</span>';
    }
    elseif ( is_product_category('obrabotka') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для обработки</span>';
    }
    elseif ( is_product_category('kalibrovka') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для калибровки</span>';
    }
    elseif ( is_product_category('vzveshivanie') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для взвешивания</span>';
    }
    elseif ( is_product_category('upakovka-protectedground') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для упаковки</span>';
    }
    elseif ( is_product_category('kalibrovka-berries-fruits') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для калибровки</span>';
    }
    elseif ( is_product_category('obrabotka-berries-fruits') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для обработки</span>';
    }
    elseif ( is_product_category('vzveshivanie-berries-fruits') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для взвешивания</span>';
    }
    elseif ( is_product_category('upakovka-berries-fruits') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для упаковки</span>';
    }
    elseif ( is_product_category('paletirovanie') ) {
        $page_title = 'Результат по запросу — <span>Оборудование для палетирования</span>';
    }
    elseif ( is_product_category('jabloki') ) {
        $page_title = 'Результат по запросу — <span>Яблоки</span>';
    }
    elseif ( is_product_category('tomaty') ) {
        $page_title = 'Результат по запросу — <span>Томаты</span>';
    }
    elseif ( is_product_category('ogurcy') ) {
        $page_title = 'Результат по запросу — <span>Огурцы</span>';
    }
    elseif ( is_product_category('kartofel') ) {
        $page_title = 'Результат по запросу — <span>Картофель</span>';
    }
    elseif ( is_product_category('salat') ) {
        $page_title = 'Результат по запросу — <span>Салат</span>';
    }
    elseif ( is_product_category('griby') ) {
        $page_title = 'Результат по запросу — <span>Грибы</span>';
    }
    elseif ( is_product_category('morkov') ) {
        $page_title = 'Результат по запросу — <span>Морковь</span>';
    }
    elseif ( is_product_category('svekla') ) {
        $page_title = 'Результат по запросу — <span>Свекла</span>';
    }
    elseif ( is_product_category('luk') ) {
        $page_title = 'Результат по запросу — <span>Лук</span>';
    }
    elseif ( is_product_category('zelen') ) {
        $page_title = 'Результат по запросу — <span>Зелень</span>';
    }
    elseif ( is_product_category('jagody') ) {
        $page_title = 'Результат по запросу — <span>Ягоды</span>';
    }
    elseif ( is_product_category('jekzotika') ) {
        $page_title = 'Результат по запросу — <span>Экзотика</span>';
    }
    elseif ( is_product_category('citrusovye') ) {
        $page_title = 'Результат по запросу — <span>Цитрусовые</span>';
    }
    elseif ( is_product_category('grushi') ) {
        $page_title = 'Результат по запросу — <span>Груши</span>';
    }
    elseif ( is_product_category('redis') ) {
        $page_title = 'Результат по запросу — <span>Редис</span>';
    }
    elseif ( is_product_category('arbuz') ) {
        $page_title = 'Результат по запросу — <span>Арбуз</span>';
    }
    elseif ( is_product_category('perec') ) {
        $page_title = 'Результат по запросу — <span>Перец</span>';
    }
    elseif ( is_product_category('chesnok') ) {
        $page_title = 'Результат по запросу — <span>Чеснок</span>';
    }
    return $page_title;
}

remove_action('wp_head', 'wp_generator');

//мультиселект на странице упаковки

add_filter('wpcf7_form_tag_data_option', function($n, $options, $args) {
  global $post;
  if( $_SERVER['REQUEST_URI'] == '/service/' ) {
    $all_ids = get_posts( array(
      'post_type' => 'post',
      'category' => '149',
      'numberposts' => -1,
      'post_status' => 'publish',
      'fields' => 'ids',
    ));
    $brands = [];
    foreach ( $all_ids as $id ) {
      $title = get_the_title($id);
      array_push($brands, $title);
    }
    //print_r($brands);
    if (in_array('brands', $options)){
      return $brands;
    }
  } else {
    $all_ids = get_posts( array(
      'post_type' => 'product',
      'numberposts' => -1,
      'post_status' => 'publish',
      'fields' => 'ids',
      'tax_query' => array(
        array(
          'taxonomy' => 'product_cat',
          'field' => 'slug',
          'terms' => 'materials',
          'operator' => 'IN',
        )
      ),
    ));
    $materials = [];
    foreach ( $all_ids as $id ) {
      $title = get_the_title($id);
      array_push($materials, $title);
    }
    if (in_array('materials', $options)){
      return $materials;
    }
  }
  return $n;
}, 10, 3);


add_action('wp_footer', 'wpmidia_activate_masked_input');
function wpmidia_activate_masked_input(){
  ?>
  <script type="text/javascript">
      jQuery( function($){
          $(".tel").mask("+7 (999) 999-99-99");
      });
  </script>
  <?php
}

// случайный вывод товаров на главной каталога

add_filter('woocommerce_default_catalog_orderby', 'misha_catalog_orderby_for_category');
 
function misha_catalog_orderby_for_category( $sort_by ) {
	if( !is_product_category('equipment') ) { 
		return $sort_by; // no changes for any page except Uncategorized
	}
	return 'rand';
}


add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail' );

function wpse_100012_override_yoast_breadcrumb_trail( $links ) {
    global $post;

    if ( (is_singular( 'post' ) && in_category(25)) || is_category(25) || is_category(150) || is_category(151) || is_category(152) || is_category(153) || is_category(154) || is_category(155) || is_category(156) || is_category(157) || is_category(158) ) {
        $breadcrumb[] = array(
            'url' => '/events/',
            'text' => 'События',
        );
        array_splice( $links, 1, -3, $breadcrumb );
    } elseif ( (is_singular( 'post' ) && in_category(137)) || is_category(137) || is_category(144) || is_category(145) || is_category(146) || is_category(147) ) {
        $breadcrumb[] = array(
            'url' => '/company/',
            'text' => 'О компании',
        );
        array_splice( $links, 1, -3, $breadcrumb );
    } elseif ( (is_singular( 'post' ) && in_category(31)) || is_category(31) || is_category(32) || is_category(33) || is_category(34) || is_category(50) || is_category(51) || is_category(138) ) {
        $breadcrumb[] = array(
            'url' => '/events/',
            'text' => 'События',
        );
        array_splice( $links, 1, -5, $breadcrumb );
    }
    foreach ($links as $value => $key) {
      if ($key['text'] == 'Мероприятия') $links[$value]['text'] = 'События';
    }
    return $links;
}

require get_template_directory() . '/includes/TaxonomyMaterials.php';
add_action('init', 'materials_register_taxonomy');

add_filter('woocommerce_single_product_zoom_enabled', '__return_false');