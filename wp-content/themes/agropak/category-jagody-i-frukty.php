<?php
/**
 * Template Name: News
 */

get_header(); ?>

    <div class="container section clients-top clients-top-with-map all-rojects-page">
        <h2 class="finance-header projects-header">Успешно выполненные проекты</h2>
        <div class="row">
            <div class="col-lg-12 map-on-main">
              <?php echo do_shortcode('[russiahtml5map id="5"] '); ?>
            </div>
        </div>

        <div class="all-projects-text">
            За 24 года работы нами реализовано более 3000 проектов различного масштаба и уровня автоматизации. В данном разделе представлены индивидуальные проекты некоторых наших клиентов. В карточке проекта отражена максимально подробная информация о каждом из них.
        </div>

    </div>

<?php include "category-projects-slider.php"; ?>
    <div class="container section" id="projectview">

      <?php
      $proj_id = 147;
      $posts = get_posts ("category=".$proj_id."&orderby=date");
      if ($posts) :
        ?>
          <div class="projects-all-wide">
              <ul class="projects-all-wrap">
                <?php
                foreach ($posts as $post) : setup_postdata ($post);
                  $cat_url = get_field('albums_preview',$post->ID);
                  ?>
                    <li>
                        <div class="photoalbum-projects-wrap">
                            <div class="photoalbum-year">
                                <div class="photoalbum-year-img"><a href="<?php the_permalink(); ?>">
                                        <img src="<?php echo $cat_url['sizes']['albums_preview']; ?>"></a>
                                </div>
                                <div class="photoalbum-year-title">
                                    <a href="<?php the_permalink(); ?>"><span><?php the_title(); ?></span></a>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
              </ul>
          </div>
          <div class="projects-all-narrow">
              <p class="before-project-slider">.</p>
              <div id="flexslider-projects" class="flexslider">
                  <ul class="slides">
                    <?php
                    foreach ($posts as $post) : setup_postdata ($post);
                      $cat_url = get_field('albums_preview',$post->ID);
                      ?>
                        <li>
                            <div class="photoalbum-projects-wrap">
                                <div class="photoalbum-year">
                                    <div class="photoalbum-year-img"><a href="<?php the_permalink(); ?>">
                                            <img src="<?php echo $cat_url['sizes']['albums_preview']; ?>"></a>
                                    </div>
                                    <div class="photoalbum-year-title">
                                        <a href="<?php the_permalink(); ?>"><span><?php the_title(); ?></span></a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                  </ul>
              </div>
          </div>

      <?php endif; ?>

    </div>





<?php
get_footer();
