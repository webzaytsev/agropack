<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package agropak
 */

get_header(); ?>

	<section id="primary" class="content-area news-page">
		<div id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post(); ?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="container">
<div class="row">
    
    <?php if ($download_program) { ?>
        <div class="col-lg-6 news-left">
            <header class="entry-header">
                <?php
                    the_title( '<h1 class="entry-title">', '</h1>' );
                        ?>
            </header>
            <div class="entry-content">
                <div class="date-record">
                          <?php the_date('d.m.Y'); ?>
                </div>
                <?php
                    the_content();
                ?>
            </div><!-- .entry-content -->
        </div>
        <div class="col-lg-6 news-right">
        	<div class="single-news-img post-thumbnail">
                <div class="news-thumbnail">
                  <?php the_post_thumbnail(); ?>
                </div>
              <?php
              $download_program = get_field_object( "download_program" );
              if ($download_program) {
                $program_id = $download_program['value'];
                $program_permalink = get_the_permalink($program_id);
              }
              $name_download = get_field_object( "name_download" );
              ?>
                <div class="">
                  <?php
                  if ( $download_program and $download_program['value'] > 0 ) : ?>
                    <a href="<?php echo $program_permalink; ?>">
                        <div class="news-button-wrap">
                      <div class="button-download">
                          <span>
                              <?php
                    if ( $name_download and $name_download['value'] !== '' ) {
                        echo $name_download['value'];
                    }
                    else {
                        echo "Скачать программу";
                    }
                              ?>
                          </span>
                      </div>
                        </div>
                    </a>
                  <?php endif; ?>
                </div>


            </div>
        </div>
    <?php } else { ?>
        <div class="col-lg-12 news-left">
            <header class="entry-header">
                <?php
                    the_title( '<h1 class="entry-title">', '</h1>' );
                        ?>
            </header>
            <div class="entry-content">
                <div class="date-record">
                          <?php the_date('d.m.Y'); ?>
                </div>
                <?php
                    the_content();
                ?>
            </div><!-- .entry-content -->
        </div>
    <?php } ?>
</div>
</div>
</article><!-- #post-## -->


            <div class="container section" id="projectview">
                <div id="flexslider-photoalbum" class="flexslider">
                    <ul class="slides">
                      <?php $images = acf_photo_gallery('photos', $post->ID);
                      if ($images) :
                        echo do_shortcode('[metaslider id="96"]');
                        foreach ($images as $image) :
                          ?>
                            <li>
                                <a href="<?php echo $image['full_image_url']?>">
                                    <div class="photoalbum-projects-wrap colorbox galery-preview">
                                        <div class="photoalbum-year">
                                            <div class="photoalbum-year-img">
                                                <div class="photo-announce-project colorbox galery-preview" style="background: url(<?php echo $image['full_image_url']?>) no-repeat center; background-size: cover;">
                                                </div>
                                            </div>
                                            <div class="photoalbum-year-title">
                                                <div class="project-pic-title"><span><?=$image['title']?></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        <?php endforeach; ?>
                      <?php endif; ?>
                    </ul>
                </div>
            </div>


		<?php
		endwhile; // End of the loop.
		?>

		</div><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
