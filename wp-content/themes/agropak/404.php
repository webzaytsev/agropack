<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Agropak
 */

get_header(); ?>

    <section id="primary" class="content-area col-md-12 col-lg-12">
        <div id="main" class="site-main" role="main">

            <section class="error-404 not-found">
                <header class="page-header error-404__header">
                    <img src="<?= get_template_directory_uri() . '/colorbox/images/404.png'; ?>" alt="404" class="error-404__img">
                </header><!-- .page-header -->

                <div class="page-content error-404__page-content">
                    <p class="error-404__page-content_text">Упс...
                        <br>Запрашиваемая вами страница не найдена
                        <br>или перенесена в другой раздел
                    </p>

                    <p class="error-404__page-content_note">Воспользуйтесь поиском сайта или напишите нам, мы вам поможем</p>

                    <?php
                    get_search_form();


                    ?>

                </div><!-- .page-content -->
            </section><!-- .error-404 -->

        </div><!-- #main -->
    </section><!-- #primary -->

<?php
get_footer();
