<?php

function materials_register_taxonomy()
{
    register_taxonomy('materials', 'product',
        array(
            'labels' => [
                'name' => 'Материалы',
                'singular_name' => 'Материал',
                'search_items' => 'Искать материалы',
                'all_items' => 'Все материалы',
                'view_item' => 'Показать материалы',
                'parent_item' => 'Parent materials',
                'parent_item_colon' => 'Parent materials:',
                'edit_item' => 'Редактировать материал',
                'update_item' => 'Обновить материал',
                'add_new_item' => 'Все материалы',
                'new_item_name' => 'Название материала',
                'menu_name' => 'Материалы',
            ],
            'hierarchical' => true,
            'sort' => true,
            'args' => array('orderby' => 'term_order'),
            'show_in_rest' => true,
            'rest_base' => 'materials',
            'rest_controller_class' => 'WP_REST_Posts_Controller',
            'show_admin_column' => true
        )
    );
}