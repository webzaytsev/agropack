<?php
/**
 * Template Name: Full Width
 */

get_header(); ?>
	<div class="top-service">
		<?php the_post_thumbnail(); ?>
	</div>

	<h1 class="service-h1">Агропак производитель оборудования в России</h1>
	<div class="container section" id="warranty-block-wrap">
		<div class="row">
			<div class="col-lg-6 company-left">
				<div class="service-main-pic leaf-information">
					<div class="row">
						<div class="col-lg-6">
							<div class="proizvodstvo-date">
								25 лет
							</div>
							<div class="proizvodstvo-text">
								Опыта и успешной работы в сельскохозяйственной отрасли позволили наладить выпуск
								собственного оборудования в Санкт-Петербурге
							</div>
						</div>
						<div class="col-lg-6">
							<div class="proizvodstvo-date">
								27 линий
							</div>
							<div class="proizvodstvo-text">
								С оборудованием Агропак, установлено и уже работает на предприятиях в России
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="proizvodstvo-date">
								2018 год
							</div>
							<div class="proizvodstvo-text last-bottom">
								Выпуск первых серийных моделей высокотехнологичного оборудования для упаковки и
								предпродажной подготовки овощей и фруктов
							</div>
						</div>
						<div class="col-lg-6">
							<div class="proizvodstvo-date">
								2021 год
							</div>
							<div class="proizvodstvo-text last-bottom">
								Разработка и внедрение специализированного софта для интеграции оборудования Агропак и
								учета производственных процессов на предприятии
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 company-right">
				<div class="service-main-block">
					<p>Наша цель - помочь небольшим агропроизводителям и начинающим предприятиям, осуществить
						автоматизацию технологических процессов и процесс упаковки, с минимальными инвестициями в начале
						пути. Автоматизация упаковочного цеха на предприятии, позволяет быстро и качественно упаковывать
						свежие овощи и фрукты без участия самих сотрудников. Координировать бесперебойную работу
						упаковочной линии и оборудования сможет один специально обученный человек.</p>
					<p class="service-last-p">Сегодня мы проектируем и производим оборудование для упаковочных линий в
						трех отраслях: открытый грунт, защищенный грунт и садоводство. Пилотные образцы нашего
						оборудования проходят многоступенчатое тестирование перед поставкой клиенту. Также, все
						производимое российское оборудование Агропак можно приобрести при помощи государственных
						программ льготного кредитования, субсидирования, лизинга или гранта – мы аккредитованы в
						национальной системе аккредитации. Подробнее о финансовых условиях можно почитать в разделе
						«Финансы».</p>
				</div>
			</div>
		</div>
	</div>

	<section class="container section">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="proizvodstvo-h2">Выберите тип интересующего оборудования</h2>
				<div class="equipment-main category-proizvodstvo equipment-catalog">
					<div class="equip-main-wrap">
						<a href="/proizvodstvo/?equipment=42#equipmentview">
							<div class="equip-main-circle">
								<div class="equip-main">
									<img src="<?php bloginfo( 'template_url' ); ?>/inc/img/equip-1.svg" alt="Открытый грунт">
								</div>
							</div>
							<div class="equip-main-link">
								Открытый грунт
							</div>
						</a>
					</div>
					<div class="equip-main-wrap">
						<a href="/proizvodstvo/?equipment=24#equipmentview">
							<div class="equip-main-circle">
								<div class="equip-main">
									<img src="<?php bloginfo( 'template_url' ); ?>/inc/img/equip-2.svg" alt="Защищенный грунт">
								</div>
							</div>
							<div class="equip-main-link">
								Защищенный грунт
							</div>
						</a>
					</div>
					<div class="equip-main-wrap">
						<a href="/proizvodstvo/?equipment=20#equipmentview">
							<div class="equip-main-circle">
								<div class="equip-main">
									<img src="<?php bloginfo( 'template_url' ); ?>/inc/img/equip-3.svg" alt="Садоводство">
								</div>
							</div>
							<div class="equip-main-link">
								Садоводство
							</div>
						</a>
					</div>
					<div class="equip-main-wrap">
						<a href="/proizvodstvo/?equipment=57#equipmentview">
							<div class="equip-main-circle">
								<div class="equip-main">
									<img src="<?php bloginfo( 'template_url' ); ?>/inc/img/equip-4.svg" alt="Ягоды и фрукты">
								</div>
							</div>
							<div class="equip-main-link">
								Другое
							</div>
						</a>
					</div>
				</div>

				<div class="slider-category-narrow">
					<?php echo do_shortcode( '[metaslider id="96"]' ); ?>
					<div id="flexslider-category" class="flexslider flexslider-proizvodstvo">
						<ul class="slides">
							<li>
								<div class="equip-main-wrap">
									<a href="/proizvodstvo/?equipment=42#equipmentview">
										<div class="equip-main-circle">
											<div class="equip-main">
												<img src="<?php bloginfo( 'template_url' ); ?>/inc/img/equip-1.svg" alt="Открытый грунт">
											</div>
										</div>
										<div class="equip-main-link">
											Открытый грунт
										</div>
									</a>
								</div>
							</li>
							<li>
								<div class="equip-main-wrap">
									<a href="/proizvodstvo/?equipment=24#equipmentview">
										<div class="equip-main-circle">
											<div class="equip-main">
												<img src="<?php bloginfo( 'template_url' ); ?>/inc/img/equip-2.svg" alt="Защищенный грунт">
											</div>
										</div>
										<div class="equip-main-link">
											Защищенный грунт
										</div>
									</a>
								</div>
							</li>
							<li>
								<div class="equip-main-wrap">
									<a href="/proizvodstvo/?equipment=20#equipmentview">
										<div class="equip-main-circle">
											<div class="equip-main">
												<img src="<?php bloginfo( 'template_url' ); ?>/inc/img/equip-3.svg" alt="Садоводство">
											</div>
										</div>
										<div class="equip-main-link">
											Садоводство
										</div>
									</a>
								</div>
							</li>
							<li>
								<div class="equip-main-wrap">
									<a href="/proizvodstvo/?equipment=57#equipmentview">
										<div class="equip-main-circle">
											<div class="equip-main">
												<img src="<?php bloginfo( 'template_url' ); ?>/inc/img/equip-4.svg" alt="Ягоды и фрукты">
											</div>
										</div>
										<div class="equip-main-link">
											Другое
										</div>
									</a>
								</div>
							</li>

						</ul>
					</div>
				</div>

				<div class="slider-category-narrow">
					<?php echo do_shortcode( '[metaslider id="96"]' ); ?>
					<div id="flexslider-category" class="flexslider flexslider-proizvodstvo">
						<ul class="slides">
							<li>
								<div class="equip-main-wrap">
									<a href="/equipment/gardening/">
										<div class="equip-main-circle">
											<div class="equip-main">
												<img src="<?php bloginfo( 'template_url' ); ?>/inc/img/equip-3.svg" alt="Садоводство">
											</div>
										</div>
										<div class="equip-main-link">
											Садоводство
										</div>
									</a>
								</div>
							</li>
							<li>
								<div class="equip-main-wrap">
									<a href="/equipment/openground/">
										<div class="equip-main-circle">
											<div class="equip-main">
												<img src="<?php bloginfo( 'template_url' ); ?>/inc/img/equip-1.svg" alt="Открытый грунт">
											</div>
										</div>
										<div class="equip-main-link">
											Открытый грунт
										</div>
									</a>
								</div>
							</li>
							<li>
								<div class="equip-main-wrap">
									<a href="/equipment/protectedground/">
										<div class="equip-main-circle">
											<div class="equip-main">
												<img src="<?php bloginfo( 'template_url' ); ?>/inc/img/equip-2.svg" alt="Защищенный грунт">
											</div>
										</div>
										<div class="equip-main-link">
											Защищенный грунт
										</div>
									</a>
								</div>
							</li>
							<li>
								<div class="equip-main-wrap">
									<a href="/equipment//berries-fruits/">
										<div class="equip-main-circle">
											<div class="equip-main">
												<img style="max-width: 75px;"
													 src="<?php bloginfo( 'template_url' ); ?>/inc/img/icon-equipment-4.svg" alt="Ягоды и фрукты">
											</div>
										</div>
										<div class="equip-main-link">
											Ягоды и фрукты
										</div>
									</a>
								</div>
							</li>

						</ul>
					</div>
				</div>
	</section>

	<section class="container section" id="equipmentview">
		<div class="row">
			<div class="col-lg-5 company-left">
				<div class="production-3d-list">
					<?php echo do_shortcode( '[metaslider id="96"]' ); ?>
					<div id="flexslider8" class="flexslider">
						<ul class="slides">
							<?php
							$cur_equipment = 19;
							if ( isset( $_GET['equipment'] ) ) {
								$cur_equipment = $_GET['equipment'];
							}

							$cur_product = 1938;
							if ( ( isset( $_GET['id'] ) ) && ( null !== ( $page = filter_input( INPUT_GET, 'id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE ) ) ) ) {
								$cur_product = $_GET['id'];
							}

							if ( ! get_post_status( $cur_product ) ) {
								$cur_product = 328;
							}
							$query    = new WP_Query(
								array(
									'post_type'      => 'product',
									'post_status'    => 'publish',
									'posts_per_page' => -1,
									'tax_query'      => array(
										array(
											'taxonomy' => 'product_cat',
											'field'    => 'term_id',
											'terms'    => array( $cur_equipment ),
										),
									),
								)
							);
							$cntslide = 0;
							while ( $query->have_posts() ) :
								$query->the_post();
								if ( get_field( '3dslider' ) == 1 ) :
									if ( $cntslide == 0 || $cntslide % 4 == 0 ) {
										echo '<li><div class="row">';
									}
									if ( get_the_post_thumbnail_url() == false ) {
										$imgsrc = '/wp-content/uploads/woocommerce-placeholder-300x300.png';
									} else {
										$imgsrc = get_the_post_thumbnail_url( '', $size = array( 300, 300 ) );
									}
									?>
									<div class="col-6">
										<div class="production-item-wrap"><a
													href="/proizvodstvo/?equipment=<?php echo $cur_equipment; ?>&id=<?php echo $query->post->ID; ?>#equipmentview">
												<div class="production-item">
													<img src="<?php echo $imgsrc; ?>"></div>
												<h3><?php echo get_the_title(); ?></h3></a></div>
									</div>
									<?php
									if ( ( $query->post_count - 1 ) == $cntslide || $cntslide == 3 || ( $cntslide > 5 && $cntslide % 4 == 0 ) ) {
										echo '</div></li>';
									}
									if ( get_field( '3dslider' ) == 1 ) {
										$cntslide++;
									}
								endif;
							endwhile;
							wp_reset_postdata();
							?>
						</ul>
						<div class="product-select-txt"><span>Посмотрите другое оборудование</span></div>
					</div>
				</div>
			</div>
			<div class="col-lg-7 company-right">
				<div class="product-model-3d__plug">
					<div class="product-model-3d__plug_img">
						<svg width="144" height="116" viewBox="0 0 144 116" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M57.9021 68.3843L0 34.952V57.7467C0 67.8777 5.53846 76.9956 14.0979 82.0611L72 115.493V92.6987C72 83.0742 66.4615 73.4498 57.9021 68.3843Z" fill="#00AD7A"/>
							<path d="M86.0978 33.4323L144 0V22.7948C144 32.9258 138.461 42.0437 129.902 47.1092L71.9999 81.048V58.2533C71.9999 48.1223 77.5384 38.4978 86.0978 33.4323Z" fill="#00AD7A"/>
						</svg>
					</div>
					<p class="product-model-3d__plug_caption">Раздел обновляется,
						<br>скоро здесь появится информация</p>
					<p class="product-model-3d__plug_text">Хотите узнать подробнее,
						<br>подпишитесь на нашу рассылку</p>
				</div>

				<?php $product = wc_get_product($cur_product); ?>

				<div class="row product-specification">
					<?php echo '<h2 class="product-title-3d">' . $product->get_title() . '</h2><hr class="product-hr">'; ?>
					<div class="col-lg-7 product-left-col">
						<div class="woocommerce">
							<?php echo $product->list_attributes(); ?>
						</div>
					</div>
					<div class="col-lg-5">
					</div>

					<div class="button-service proizvodstvo-form proizvodstvo-order">
						<a href="#">Отправить запрос</a>
					</div>

				</div>
			</div>
		</div>
	</section>


<?php
get_footer();
