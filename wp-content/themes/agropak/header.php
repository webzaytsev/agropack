<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package agropak
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
<!--<script type='text/javascript' src='https://dev.agropak.ru/wp-content/plugins/ml-slider/assets/sliders/flexslider/jquery.flexslider.min.js?ver=3.20.3' id='metaslider-flex-slider-js'></script>-->
</head>

<body <?php body_class(); ?>>

<?php 

    // WordPress 5.2 wp_body_open implementation
    if ( function_exists( 'wp_body_open' ) ) {
        wp_body_open();
    } else {
        do_action( 'wp_body_open' );
    }

?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'agropak' ); ?></a>
    <?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
    
    <header id="masthead" class="site-header navbar-static-top top-fixed <?php echo agropak_bg_class(); ?>" role="banner" style="top: 0;position: sticky;z-index: 9999;">
        <div class="search_bl">
            <div class="container container_padding">
                <form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>" style="width: 100%;">
                    <input type="search" class="moving-input" placeholder="Введите ваш запрос и нажмите Enter" name="s"  value="<?php echo esc_attr( get_search_query() ); ?>">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12 0C15.1826 0 18.2348 1.26428 20.4853 3.51472C22.7357 5.76516 24 8.8174 24 12C24 15.1826 22.7357 18.2348 20.4853 20.4853C18.2348 22.7357 15.1826 24 12 24C8.8174 24 5.76516 22.7357 3.51472 20.4853C1.26428 18.2348 0 15.1826 0 12C0 8.8174 1.26428 5.76516 3.51472 3.51472C5.76516 1.26428 8.8174 0 12 0V0ZM12 10.5446L9.21257 7.75714C9.01957 7.56414 8.7578 7.45571 8.48486 7.45571C8.21191 7.45571 7.95014 7.56414 7.75714 7.75714C7.56414 7.95014 7.45571 8.21191 7.45571 8.48486C7.45571 8.7578 7.56414 9.01957 7.75714 9.21257L10.5446 12L7.75714 14.7874C7.66158 14.883 7.58577 14.9964 7.53405 15.1213C7.48233 15.2462 7.45571 15.38 7.45571 15.5151C7.45571 15.6503 7.48233 15.7841 7.53405 15.909C7.58577 16.0338 7.66158 16.1473 7.75714 16.2429C7.85271 16.3384 7.96616 16.4142 8.09102 16.4659C8.21588 16.5177 8.34971 16.5443 8.48486 16.5443C8.62001 16.5443 8.75383 16.5177 8.87869 16.4659C9.00355 16.4142 9.11701 16.3384 9.21257 16.2429L12 13.4554L14.7874 16.2429C14.883 16.3384 14.9964 16.4142 15.1213 16.4659C15.2462 16.5177 15.38 16.5443 15.5151 16.5443C15.6503 16.5443 15.7841 16.5177 15.909 16.4659C16.0338 16.4142 16.1473 16.3384 16.2429 16.2429C16.3384 16.1473 16.4142 16.0338 16.4659 15.909C16.5177 15.7841 16.5443 15.6503 16.5443 15.5151C16.5443 15.38 16.5177 15.2462 16.4659 15.1213C16.4142 14.9964 16.3384 14.883 16.2429 14.7874L13.4554 12L16.2429 9.21257C16.3384 9.11701 16.4142 9.00355 16.4659 8.87869C16.5177 8.75383 16.5443 8.62001 16.5443 8.48486C16.5443 8.34971 16.5177 8.21588 16.4659 8.09102C16.4142 7.96616 16.3384 7.85271 16.2429 7.75714C16.1473 7.66158 16.0338 7.58577 15.909 7.53405C15.7841 7.48233 15.6503 7.45571 15.5151 7.45571C15.38 7.45571 15.2462 7.48233 15.1213 7.53405C14.9964 7.58577 14.883 7.66158 14.7874 7.75714L12 10.5446Z" fill="#00AD7A"/>
                    </svg>
                </form>
            </div>
        </div>
        <div class="container topsite">
            <nav class="navbar navbar-expand-xl p-0 new_menu_navbar">
                <div class="navbar-brand">
                   <a href="/"><img src="<?php bloginfo("template_url"); ?>/inc/img/agropak.svg" alt="Агропак логотип"></a>
                </div>
                

                <div class="new_menu">
                    <?php
                    wp_nav_menu(array(
                    'theme_location'    => 'primary',
                    'container'       => 'div',
                    'container_id'    => 'main-nav',
                    'container_class' => 'collapse navbar-collapse justify-content-start',
                    'menu_id'         => false,
                    'menu_class'      => 'navbar-nav',
                    'depth'           => 2,
                    'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                    'walker'          => new wp_bootstrap_navwalker()
                    ));
                    ?>

                    <div class="search-top-wrap">
                        <div class="moving-search">
                            <form role="search" method="get" class="search-form slider-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                                <i class="fa fa-search"></i>
                                
                            </form>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" >
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>
                </div>

                <div class="topphone-wrap topphone-wrap_pc">
                  <div class="topphone-digits">
                    <div class="topphone-number">
                        <img src="<?php bloginfo("template_url"); ?>/inc/img/phone-pic.svg">
                        <a href="tel:88005051930">8 800 505-19-30</a>
                    </div>
                    <div class="topphone-descr">
                        Звонок по России бесплатный
                    </div>
                  </div>
                </div>
            </nav>
        </div>
        <div class="soc-expanded">
            <div class="soc-expanded-messendger">
                <div class="we-soc">Удобная обратная связь</div>
                <div class="soc-pics">
                    <a href="https://wa.me/88005051930" rel="nofollow" target="_blank"><img src="/wp-content/themes/agropak/inc/img/soc-pic-2.png"></a>
                    <a href="tg://resolve?domain=agropaklogin" rel="nofollow" target="_blank"><img src="/wp-content/themes/agropak/inc/img/soc-pic-4.png"></a>
                </div>
            </div>
            <div class="soc-expanded-pic">
                <div class="we-soc">Мы в социальной сети</div>
                <div class="social-social">
                    <div class="social-yt"><a href="https://www.youtube.com/user/agropakru" target="_blank"><img src="<?php bloginfo("template_url"); ?>/inc/img/youtube.svg"></a></div>
                    <div class="social-in"><a href="https://www.instagram.com/agropak.ru/" target="_blank"><img src="<?php bloginfo("template_url"); ?>/inc/img/instagram.svg"></a></div>
                    <div class="social-fb"><a href="https://www.facebook.com/agropak.ru"><img src="<?php bloginfo("template_url"); ?>/inc/img/facebook.svg"></a></div>
                </div>
            </div>
        </div>
	</header><!-- #masthead -->

    <?php if(is_front_page()): ?>
	<div class="main-slider-wrap">
<?php echo do_shortcode('[metaslider id="27"]'); ?>
	    <div class="search-wrap">
               <div class="search-social">
<div class="social-yt"><a href="https://vk.com/agropak" target="_blank"><img src="<?php bloginfo("template_url"); ?>/inc/img/vkontakte.svg"></a></div>
<div class="social-in"><a href="https://www.instagram.com/agropak.ru/" target="_blank"><img src="<?php bloginfo("template_url"); ?>/inc/img/instagram.svg"></a></div>
<div class="social-yt"><a href="https://www.youtube.com/user/agropakru" target="_blank"><img src="<?php bloginfo("template_url"); ?>/inc/img/youtube.svg"></a></div>
<div class="social-fb"><a href="https://www.facebook.com/agropak.ru"><img src="<?php bloginfo("template_url"); ?>/inc/img/facebook.svg"></a></div>
                </div>
	    </div>
	</div>

    <?php endif; ?>

    <?php if(!is_front_page()): ?>
	<div class="container header-category">
             <div id="header-bottom">
<?php
/* breadcrumb Yoast */
if ( function_exists( 'yoast_breadcrumb' ) ) :
   yoast_breadcrumb( '<div id="breadcrumbs">', '</div>' );
endif;
$parentid = get_queried_object_id();

if ( is_product_category() ){

  global $wp_query;
  $cat = $wp_query->get_queried_object();
  $thumbnail_id = get_term_meta($cat->term_id, 'thumbnail_id', true);
  $image = wp_get_attachment_url( $thumbnail_id );
  if ($image != "") {
    echo "<div class='top-category-wrap'><img class='top-category-img' src='{$image}' alt='' /></div>";
  }
  if ( $parentid === 22 ) {
    echo ' <h2 class="choose-packing">Выберите продукт для упаковки</h2>';
  }
}
?>
<?php
    if ($parentid > 1) {
	$args = array(
	    'parent' => $parentid
	);
	$terms = get_terms( 'product_cat', $args );

	$ancestors = get_ancestors( $parentid, 'product_cat' );
	if (empty( $terms )) {
	    $args = array(
	        'parent' => $ancestors[0]
	    );
            if (isset($args['parent'])) {
	      $terms = get_terms( 'product_cat', $args );
            }
	}
	if ( $terms && $parentid !== 19 ) {
	    echo '<ul class="category-list">';
	        foreach ( $terms as $term ) {
$green_pic = get_field('green_pic', 'category_'.$term->term_id.'');
$white_pic = get_field('white_pic', 'category_'.$term->term_id.'');
?>
<style>
.category-item a.<?=$term->slug?> span:before {
  background: url(<?php echo $green_pic; ?>) no-repeat;
}
.category-item a.<?=$term->slug?>:hover span:before {
  background: url(<?php echo $white_pic; ?>) no-repeat;
}
</style>
<?php
	            if ($parentid == $term->term_taxonomy_id) { $actineclass = "active"; } else { $actineclass = ""; }
	            echo '<li class="category-item">';
	                    echo '<a class="'.$term->slug.'" href="' .  esc_url( get_term_link( $term ) ) . '" class="' . $actineclass . '"><span>';
	                        echo $term->name;
	                    echo '</span></a>';                        
	            echo '</li>';                                                        
	    }
	    echo '</ul>';
	} elseif ($terms && $parentid === 19) { ?>
        <h2 class="choose-department">Выберите отрасль</h2>
        <div class="equipment-main equipment-catalog">
            <div class="equip-main-wrap">
                <a href="/equipment/gardening/">
                    <div class="equip-main-circle">
                        <div class="equip-main">
                            <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-3.svg" alt="Садоводство">
                        </div>
                    </div>
                    <div class="equip-main-link">
                        Садоводство
                    </div>
                </a>
            </div>
            <div class="equip-main-wrap">
                <a href="/equipment/openground/">
                <div class="equip-main-circle">
                    <div class="equip-main">
                       <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-1.svg" alt="Открытый грунт">
                    </div>
                </div>
                <div class="equip-main-link">
                    Открытый грунт
                </div>
                </a>
            </div>
            <div class="equip-main-wrap">
                <a href="/equipment/protectedground/">
                    <div class="equip-main-circle">
                        <div class="equip-main">
                            <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-2.svg" alt="Защищенный грунт">
                        </div>
                    </div>
                    <div class="equip-main-link">
                        Защищенный грунт
                    </div>
                </a>
            </div>
            <div class="equip-main-wrap">
                <a href="/equipment//berries-fruits/">
                    <div class="equip-main-circle">
                        <div class="equip-main">
                            <img style="max-width: 75px;" src="<?php bloginfo("template_url"); ?>/inc/img/icon-equipment-4.svg" alt="Ягоды и фрукты">
                        </div>
                    </div>
                    <div class="equip-main-link">
                        Ягоды и фрукты
                    </div>
                </a>
            </div>
        </div>
        <div class="slider-category-narrow">
          <?php echo do_shortcode('[metaslider id="96"]');?>
            <div id="flexslider-category" class="flexslider">
                <ul class="slides">
                    <li>
                        <div class="equip-main-wrap">
                            <a href="/equipment/gardening/">
                                <div class="equip-main-circle">
                                    <div class="equip-main">
                                        <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-3.svg" alt="Садоводство">
                                    </div>
                                </div>
                                <div class="equip-main-link">
                                    Садоводство
                                </div>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div class="equip-main-wrap">
                            <a href="/equipment/openground/">
                                <div class="equip-main-circle">
                                    <div class="equip-main">
                                        <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-1.svg" alt="Открытый грунт">
                                    </div>
                                </div>
                                <div class="equip-main-link">
                                    Открытый грунт
                                </div>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div class="equip-main-wrap">
                            <a href="/equipment/protectedground/">
                                <div class="equip-main-circle">
                                    <div class="equip-main">
                                        <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-2.svg" alt="Защищенный грунт">
                                    </div>
                                </div>
                                <div class="equip-main-link">
                                    Защищенный грунт
                                </div>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div class="equip-main-wrap">
                            <a href="/equipment//berries-fruits/">
                                <div class="equip-main-circle">
                                    <div class="equip-main">
                                        <img style="max-width: 75px;" src="<?php bloginfo("template_url"); ?>/inc/img/icon-equipment-4.svg" alt="Ягоды и фрукты">
                                    </div>
                                </div>
                                <div class="equip-main-link">
                                    Ягоды и фрукты
                                </div>
                            </a>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
	    <?php
    }
    }
?>
            </div>
	</div>
    <?php endif; ?>


	<div id="content" class="site-content">
		<div class="container">
			<div class="row">
                <?php endif; ?>