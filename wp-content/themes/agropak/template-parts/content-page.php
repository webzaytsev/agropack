<?php
/**
 * Template part for displaying page content in page.php
 * @package agropak
 */
global $wp;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
    $enable_vc = get_post_meta(get_the_ID(), '_wpb_vc_js_status', true);
    if(!$enable_vc ) {
    ?>
	<?php 
		if (strpos($wp->request, 'events') !== false ) : 
	?>

        <div class="container section event-news-block">
            <div class="row">
<?php
// top news
    $posts_news = get_posts( array(
      'post_type' => 'post',
      'category' => '25',
      'orderby' => 'date',
      'numberposts' => -1,
      'post_status' => 'publish',
    ));
$cnt_top_news = 0;
function break_text($text, $length){
    if(strlen($text)<$length+10) {echo $text; return $text;}//don't cut if too short
    $break_pos = strpos($text, ' ', $length);//find next space after desired length
    $visible = substr($text, 0, $break_pos);
    echo balanceTags($visible) . " …";
}
foreach ($posts_news as $post):
  if (get_field('top_news') == 1 ) :
?>
                <div class="col-lg-6<?php if ($cnt_top_news == 0) echo " news-left"; else echo " news-right"; ?>">
                    <div class="news-main-big-wrap">
      <div class="news-main-block-big news-second">
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <div class="news-main-title-big"><a href="<?php the_permalink(); ?>"><?php break_text(get_the_title(), 140); ?></a></div>
          <div class="news-main-date-big"><?php echo the_time('d.m.y');  ?></div>
          <div class="news-main-preview-big"><p><?php break_text(preg_replace('/<[^>]*>/', '', $post->post_content), 310); ?></p></div>
        </div>
<div class="button-grey-green">
  <a href="<?php the_permalink(); ?>">Подробнее</a>
                     <div class="news-main-block-leaf">
                     </div>
</div>
      </div>
      <div class="news-main-img-big" style="background: url('<?php echo the_post_thumbnail_url( $id ); ?>');   
           background-position: center center; background-size: cover;">
           <div class="news-main-block-leaf">
           </div>
      </div>
                    </div>
                </div>

<?php
$cnt_top_news ++;
         if ($cnt_top_news == 2 ) {
             break;
         }
  endif;
endforeach;
?>
            </div>
        </div>

        <div class="container section news-events three-news">
            <div class="row">
<?php
// news block
$posts_news = get_posts(array(
  'numberposts' => 3,
  'category'   => '25',
));
foreach ($posts_news as $post): ?>
                <div class="col-lg-4 news-main-block">
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <div class="news-main-title"><a href="<?php the_permalink(); ?>"><?php break_text(get_the_title(), 140); ?></a></div>
          <div class="news-main-date"><?php the_time('d.m.y') ?></div>
          <div class="news-main-preview"><?php break_text(preg_replace('/<[^>]*>/', '', $post->post_content), 310); ?></div>
        </div>
                </div>
<?php
endforeach;
?>
            </div>
<div class="subscribe-news">
<div class="news-main-all-button events-photoalbums button-in-events">
  <a href="/news" class="see-archive">Показать все новости</a>
</div>
</div>
        </div>
<div class="photogallery-wrap">
<div class="photogallery-title">
<h2>Фотогалерея</h2>
</div>
</div>
		<?php
echo do_shortcode('[metaslider id="96"]');
?>
<div id="flexslider-photoalbum" class="flexslider">
  <ul class="slides">
<?php
$cat_data = get_categories( array( 'parent' => 31, 'hide_empty' => 0, 'order' => 'DSC' ) );
if ( $cat_data ) {
  $photocount = 0; 
  foreach ( $cat_data as $one_cat_data) :
    $albums_cats = get_posts('category='.$one_cat_data->term_id.'&numberposts=3');
    foreach ( $albums_cats as $albums_cat) :
      $cat_url = get_field('albums_preview',$albums_cat->ID);
?>
    <li>
      <div class="photoalbum-year-wrap">
      <div class="photoalbum-year">
        <div class="photoalbum-year-img"><a href="<?php echo get_permalink($albums_cat->ID); ?>">
          <img src="<?php echo $cat_url['sizes']['albums_preview']; ?>"></a>
        </div>
        <div class="photoalbum-year-title">
           <a href="<?php echo get_permalink($albums_cat->ID); ?>"><span><?php echo $albums_cat->post_title; ?></span></a>
        </div>
      </div>  
      </div>  
    </li>
<?php
    endforeach;
    if ($photocount == 4) {
      break;
    } 
    $photocount++;
  endforeach;
  }
?>
  </ul>
</div>
	<?php endif ?>

        <div class="subscribe-news">
            <div class="news-main-all-button events-photoalbums button-in-events">
                <a href="/photoalbums/" class="see-archive">Посмотреть архив</a>
            </div>
        </div>

    <header class="entry-header">
		<?php 
if (strpos($wp->request, 'events') === false) { 
the_title( '<h1 class="entry-title">', '</h1>' ); 
}
		?>
	</header><!-- .entry-header -->
    <?php } ?>

	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'agropak' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() && !$enable_vc ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'agropak' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>

  <?php
  if (strpos($wp->request, 'events') !== false ) :
    $posts = get_posts( array(
      'numberposts' => 5,
      'category'    => 56,
      'orderby'     => 'date',
      'order'       => 'DESC',
      'include'     => array(),
      'exclude'     => array(),
      'meta_key'    => '',
      'meta_value'  =>'',
      'post_type'   => 'post',
      'suppress_filters' => true,
    ) );
  ?>
<div class="photogallery-wrap videogallery-wrap">
<div class="photogallery-title">
<h2>Видеогалерея</h2>
</div>
</div>
      <div id="flexslider-video" class="flexslider">
          <ul class="slides">
              <?php
                foreach( $posts as $post ) :
              ?>
              <li>
                <div class="video-clip">
                    <?php echo $post->post_content; ?>
                </div>
              </li>
            <?php
              endforeach;
            ?>
          </ul>
      </div>

  <?php endif; ?>

</article><!-- #post-## -->
