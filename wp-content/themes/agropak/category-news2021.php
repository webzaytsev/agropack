<?php
/**
* Template Name: News
 */

get_header(); ?>

<?php
function break_text($text, $length){
    if(strlen($text)<$length+10) {echo $text; return $text;}
    $break_pos = strpos($text, ' ', $length);
    $visible = substr($text, 0, $break_pos);
    echo balanceTags($visible) . " …";
}
?>



<div class="news-picker">
<?php 
global $wp;
$arr_url=explode("/",home_url( $wp->request ));
$args = array('child_of' => 25, 'order' => 'DSC',);
$subcategories = get_categories( $args );
if ($subcategories) : 
?>
<div id="cityselect">
<select onchange="location=value" class="cityselect">
<?php foreach ($subcategories as $child) : 
$selected = "";
if ($arr_url[count($arr_url)-1] == $child->slug) $selected = " selected";
?>
<option value="/news/<?php echo $child->slug;?>"<?php echo $selected?>><?php echo $child->name;?></option>
<?php endforeach; ?>
</select>
</div>
<?php endif; ?>
</div>



        <div class="container section">
            <div class="row">
<?php 
    $posts = get_posts( array(
      'post_type' => 'post',
      'category' => '157',
      'orderby' => 'date',
      'numberposts' => -1,
      'post_status' => 'publish',
    ));
?> 
<?php if ($posts) : ?>
<?php $cnt_top_news = 1; ?>
<?php foreach ($posts as $post) : setup_postdata ($post); ?>
                <div class="col-lg-6<?php if($cnt_top_news % 2 === 0) echo " news-right"; else echo " news-left"; ?>">
                    <div class="news-main-big-wrap">
      <div class="news-main-block-big news-second">
        <div id="post-<?php $id; ?>" <?php post_class(); ?>>
          <div class="news-main-title-big"><b class="title"><a href="<?php the_permalink(); ?>"><?php break_text(get_the_title(), 140); ?></a></b></div>
          <div class="news-main-date-big"><?php the_time('d.m.y') ?></div>
          <div class="news-main-preview-big"><?php break_text(preg_replace('/<[^>]*>/', '', get_the_content()), 310); ?></div>
        </div>
<div class="button-grey-green">
  <a href="<?php the_permalink(); ?>">Подробнее</a>
                     <div class="news-main-block-leaf">
                     </div>
</div>
      </div>

      <div class="news-main-img-big" style="background: url('<?php echo the_post_thumbnail_url( $id ); ?>');   
           background-position: center center; background-size: cover;">
           <div class="news-main-block-leaf">
           </div>
      </div>
                    </div>
                </div>
<?php $cnt_top_news ++; ?>

<?php endforeach; ?>
<?php endif; ?>


            </div>
        </div>




<?php
get_footer();
