<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package agropak
 */

get_header(); ?>

	<section id="primary" class="content-area news-page">
		<div id="main" class="site-main" role="main">
		<?php
		while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php
$project = get_field_object('project_category');
$cur_category = $project['value'][0];
if ($cur_category == 1) {$category_name = 'Открытый грунт'; $category_img = '/wp-content/themes/agropak/inc/img/equip-1.svg';}
else if ($cur_category == 2) {$category_name = 'Защищенный грунт'; $category_img = '/wp-content/themes/agropak/inc/img/equip-2.svg';}
else if ($cur_category == 3) {$category_name = 'Садоводство'; $category_img = '/wp-content/themes/agropak/inc/img/equip-3.svg';}
else if ($cur_category == 4) {$category_name = 'Другое'; $category_img = '/wp-content/themes/agropak/inc/img/equip-4.svg';}
?>
<div class="container">
<div class="row">
    <div class="col-lg-6 news-left">
        <div class="project-category-sign">
<img src="<?=$category_img?>">
	</div>
	<header class="entry-header project-header">
		<?php
			the_title( '<h1 class="entry-title">', '</h1>' );
                ?>
	</header>
	<div class="entry-content project-page">
		<?php
			the_content();
		?>
	</div><!-- .entry-content -->
      <?php $images = acf_photo_gallery('photos', $post->ID);
      if (count($images) > 0 ) :?>
	<h2 class="finance-header before-project-img" style="text-transform: uppercase;">Фотогалерея проекта</h2>
      <?php
      endif;
      ?>
    </div>
    <div class="col-lg-6 news-right">
	<div class="post-thumbnail text-center">
	    <div class="project-thumbnail">
		<?php the_post_thumbnail(); ?>
	    </div>
	</div>
    </div>
</div>

                     <?  echo do_shortcode('[metaslider id="96"]'); ?>
                <div id="flexslider-photoalbum" class="flexslider">
                    <ul class="slides">
                      <?php $images = acf_photo_gallery('photos', $post->ID);
                      if ($images) :
                        foreach ($images as $image) :
                            ?>
                              <li>
                                  <a href="<?php echo $image['full_image_url']?>">
                                  <div class="photoalbum-projects-wrap colorbox galery-preview">
                                      <div class="photoalbum-year">
                                          <div class="photoalbum-year-img">
                                                  <div class="photo-announce-project colorbox galery-preview" style="background: url(<?php echo $image['full_image_url']?>) no-repeat center; background-size: cover;">
                                                  </div>
                                          </div>
                                          <div class="photoalbum-year-title">
                                              <div class="project-pic-title"><span><?=$image['title']?></span></div>
                                          </div>
                                      </div>
                                  </div>
                                  </a>
                              </li>
                        <?php endforeach; ?>
                      <?php endif; ?>
                    </ul>
                </div>

</div>

</article><!-- #post-## -->

		<?php
		endwhile;
		?>
		</div><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
