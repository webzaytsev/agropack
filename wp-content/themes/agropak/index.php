<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package agropak
 */

get_header(); ?>
	<section id="primary" class="content-area col-sm-12 col-md-12 col-lg-12">
		<div id="main" class="site-main" role="main">
        <div class="main-h1">
		<h1>Все для упаковки<br>свежих овощей и фруктов!</h1>
        </div>
            <div class="feedback-wrap feedback-form feedback-main feedback-main-top">
                <a href="/">Обратная связь</a>
            </div>
		</div>
	</section>
    <section id="primary" class="content-area col-sm-12 col-md-12 col-lg-12">
        <?php echo do_shortcode('[metaslider id="96"]');?>
        <div class="slider-main-narrow">
            <div class="pics-main">
                <div id="flexslider-main" class="flexslider">
                    <ul class="slides">
                        <li>
                            <div class="pic-main-wrap pic-main-wrap-mobile">
                                <a href="/service/">
                                    <div class="pic-main-circle">
                                        <div class="pic-main">
                                            <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-main-1.svg" alt="Инжиниринг">
                                        </div>
                                    </div>
                                </a>
                                <div class="pic-main-link">
                                    <a href="/service/">Инжиниринг</a>
                                </div>
                                <div class="pic-arrow pic-arrow-mobile">
                                    <div class="pic-arrow-line">
                                        <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-arrow.svg" alt="Агропак">
                                    </div>
                                    <div class="pic-arrow-line2">
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="pic-main-wrap pic-main-wrap-mobile">
                                <a href="/proizvodstvo/">
                                    <div class="pic-main-circle">
                                        <div class="pic-main">
                                            <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-main-2.svg" alt="Производство">
                                        </div>
                                    </div>
                                </a>
                                <div class="pic-main-link">
                                    <a href="/proizvodstvo/">Производство</a>
                                </div>
                                <div class="pic-arrow pic-arrow-mobile">
                                    <div class="pic-arrow-line">
                                        <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-arrow.svg" alt="Агропак">
                                    </div>
                                    <div class="pic-arrow-line2">
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="pic-main-wrap pic-main-wrap-mobile">
                                <a href="/equipment/gardening/">
                                    <div class="pic-main-circle">
                                        <div class="pic-main">
                                            <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-main-3.svg" alt="Оборудование">
                                        </div>
                                    </div>
                                </a>
                                <div class="pic-main-link">
                                    <a href="/equipment/gardening/">Оборудование</a>
                                </div>
                                <div class="pic-arrow pic-arrow-mobile">
                                    <div class="pic-arrow-line">
                                        <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-arrow.svg" alt="Агропак">
                                    </div>
                                    <div class="pic-arrow-line2">
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="pic-main-wrap pic-main-wrap-mobile">
                                <a href="/upakovka/">
                                    <div class="pic-main-circle">
                                        <div class="pic-main">
                                            <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-main-4.svg" alt="Упаковка">
                                        </div>
                                    </div>
                                </a>
                                <div class="pic-main-link">
                                    <a href="/upakovka/">Упаковка</a>
                                </div>
                                <div class="pic-arrow pic-arrow-mobile">
                                    <div class="pic-arrow-line">
                                        <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-arrow.svg" alt="Агропак">
                                    </div>
                                    <div class="pic-arrow-line2">
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="pic-main-wrap pic-main-wrap-mobile">
                                <a href="/service/">
                                    <div class="pic-main-circle">
                                        <div class="pic-main">
                                            <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-main-5.svg" alt="Сервис">
                                        </div>
                                    </div>
                                </a>
                                <div class="pic-main-link">
                                    <a href="/service/">Сервис</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

        <div class="container section pics-top">
            <div class="row">
                <div class="col-lg-12">
                    <div class="slider-main-wide">
                        <div class="pics-main">
                            <div class="pic-main-wrap">
                                <a href="/service/">
                                    <div class="pic-main-circle">
                                        <div class="pic-main">
                                            <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-main-1.svg" alt="Инжиниринг">
                                        </div>
                                    </div>
                                </a>
                                <div class="pic-main-link">
                                    <a href="/service/">Инжиниринг</a>
                                </div>
                            </div>
                            <div class="pic-main-wrap">
                                <div class="pic-arrow">
                                    <div class="pic-arrow-line">
                                        <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-arrow.svg" alt="Агропак">
                                    </div>
                                    <div class="pic-arrow-line2">
                                    </div>
                                </div>
                            </div>
                            <div class="pic-main-wrap">
                                <a href="/proizvodstvo/">
                                    <div class="pic-main-circle">
                                        <div class="pic-main">
                                            <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-main-2.svg" alt="Производство">
                                        </div>
                                    </div>
                                </a>
                                <div class="pic-main-link">
                                    <a href="/proizvodstvo/">Производство</a>
                                </div>
                            </div>
                            <div class="pic-main-wrap">
                                <div class="pic-arrow">
                                    <div class="pic-arrow-line">
                                        <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-arrow.svg" alt="Агропак">
                                    </div>
                                    <div class="pic-arrow-line2">
                                    </div>
                                </div>
                            </div>
                            <div class="pic-main-wrap">
                                <a href="/equipment/gardening/">
                                    <div class="pic-main-circle">
                                        <div class="pic-main">
                                            <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-main-3.svg" alt="Оборудование">
                                        </div>
                                    </div>
                                </a>
                                <div class="pic-main-link">
                                    <a href="/equipment/gardening/">Оборудование</a>
                                </div>
                            </div>
                            <div class="pic-main-wrap">
                                <div class="pic-arrow">
                                    <div class="pic-arrow-line">
                                        <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-arrow.svg" alt="Агропак">
                                    </div>
                                    <div class="pic-arrow-line2">
                                    </div>
                                </div>
                            </div>
                            <div class="pic-main-wrap">
                                <a href="/upakovka/">
                                    <div class="pic-main-circle">
                                        <div class="pic-main">
                                            <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-main-4.svg" alt="Упаковка">
                                        </div>
                                    </div>
                                </a>
                                <div class="pic-main-link">
                                    <a href="/upakovka/">Упаковка</a>
                                </div>
                            </div>
                            <div class="pic-main-wrap">
                                <div class="pic-arrow">
                                    <div class="pic-arrow-line">
                                        <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-arrow.svg" alt="Агропак">
                                    </div>
                                    <div class="pic-arrow-line2">
                                    </div>
                                </div>
                            </div>
                            <div class="pic-main-wrap">
                                <a href="/service/">
                                    <div class="pic-main-circle">
                                        <div class="pic-main">
                                            <img src="<?php bloginfo("template_url"); ?>/inc/img/pic-main-5.svg" alt="Сервис">
                                        </div>
                                    </div>
                                </a>
                                <div class="pic-main-link">
                                    <a href="/service/">Сервис</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="container section company-block">
            <div class="row">
                <div class="col-lg-8 news-left">
                    <div class="about-main">
                    <div class="about-main-txt">
<h2 class="about-block">Агропак российская торгово-производственная компания</h2>
                        <p>Наша цель – помочь агропроизводителям увеличить мощность предприятия и автоматизировать процесс упаковки свежих овощей и фруктов. С 1997 года мы развиваем рынок розничной упаковки, с 2002 года занимается проектированием, продажами и обслуживанием оборудования. В 2011 году запустили производство упаковочного материала «Комбиполотно». С 2018 года производим упаковочное оборудование и комплектующие для фасовочных линий. В 2020 году запустили направление по разработке софта для контроля технологических процессов агропредприятия.
                        </p>
<div class="button-grey-green">
  <a href="/company/">Подробнее</a>
</div>
                    </div>
                    <div class="about-main-img">
                    </div>
                    </div>
                </div>
                <div class="col-lg-4 news-right">
                    <div class="net-buy">
                        <div class="net-buy-left">
                            <h2 class="main-block-h2">Сетка–мешок</h2>
                            <div class="net-buy-txt">
                                Упаковка для ручной фасовки картофеля и овощей
                            </div>
                            <div class="button-net">
                                <div class="button-service">
                                    <a href="https://setka.agropak.ru" rel="nofollow" target="_blank">Купить</a>
                                </div>
                            </div>
                        </div>
                        <div class="net-buy-right">
                            <img src="/wp-content/uploads/2020/11/setka1.png">
                        </div>
                    </div>

<?php
function break_text($text, $length){
    if(strlen($text)<$length+10) {echo $text; return $text;}
    $break_pos = strpos($text, ' ', $length);
    $visible = substr($text, 0, $break_pos);
    echo balanceTags($visible) . " …";
}
?>

    <?php
        $today = date("Y-m-d");
        $posts = get_posts(array(
            'numberposts' => 10,
            'post_type'   => 'tribe_events',
            'orderby' => array('_EventStartDate' => 'DESC'),
            'meta_query' => array(
                array(
                    'key' => '_EventStartDate',
                    'value' => $today,
                    'compare' => '>='
                )
            ),
        ));
if (count($posts) < 2) {
        $today_before = date('Y-m-d', strtotime("-31 day"));
        $posts = get_posts(array(
            'numberposts' => 10,
            'post_type'   => 'tribe_events',
            'orderby' => array('_EventStartDate' => 'DESC'),
            'meta_query' => array(
                array(
                    'key' => '_EventStartDate',
                    'value' => $today_before,
                    'compare' => '>='
                )
            ),
        ));
}

$_monthsList = array(
"1"=>"Январь","2"=>"Февраль","3"=>"Март",
"4"=>"Апрель","5"=>"Май", "6"=>"Июнь",
"7"=>"Июль","8"=>"Август","9"=>"Сентябрь",
"10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь");
     ?>
                  <?php if (!empty($posts)): ?>
<?php
$li_step = " even";
if ((count($posts) % 2) == 0) {
$cnt_li=1;
} else {$cnt_li=2;}

?>
                <div class="flip-slider-wrap">
                    <div class="flip-slider-back">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 15.96 15.96">
                            <path d="M13,16H3a3,3,0,0,1-3-3V3A3,3,0,0,1,3,0H13a3,3,0,0,1,3,3V13A3,3,0,0,1,13,16Z" style="fill:#556978;fill-rule:evenodd"/>
                            <path d="M13.8,13.79H2.17V2.16H13.81V13.79Zm-11.13-.5H13.31V2.66H2.67Z" style="fill:#fff"/>
                            <text transform="translate(5.3 11.02)" style="isolation:isolate;font-size:8.731300354003906px;fill:#fff;font-family:ArialMT, Arial">7</text>
                        </svg>
                        Календарь событий
                    </div>
                    <div class="flip-slider">
                        <div class="flip-frame">
                            <div class="flip">
                                <?php foreach ($posts as $post): ?>
                                  <?php
                                  $ev_img = get_field('event_cover',$post->ID)['url'];
                                  if (empty($ev_img)) {
                                    $ev_img = '/wp-content/themes/agropak/inc/img/event-sl-1.svg';
                                  }
                                  $timestamp = strtotime(get_post_meta($post->ID, '_EventStartDate', true ));
                                  $ev_day = date('d', $timestamp);
                                  $ev_month = (int)date('m', $timestamp);
                                  $ev_month_txt = $_monthsList[$ev_month];
                                  $tribe_end = tribe_get_event($post->ID)->dates->end;
                                  $tribe_end_month = (int)$tribe_end->format_i18n('m');
                                  $tribe_end_day = $tribe_end->format_i18n('d');
if(($cnt_li % 2) == 0) {
$li_step = " odd";
} else {
$li_step = " even";
}
                                  ?>
                                  <div class="slide">
                                    <h3 class="event-main-title"><a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></h3>
                                        <div class="event-l">
                                            <div class="event-date-wrap">
                                                <div class="event-main-date">
                                                    <p class="event__date-day">
                                                        <?php echo $ev_day; ?>
                                                    </p>
                                                    <p class="event__date-end_day">
                                                        <?php echo $tribe_end_day; ?>
                                                    </p>
                                                </div>
                                                <div class="event-main-month">
                                                    <?php echo $ev_month_txt;
                                                    if ($ev_month !== $tribe_end_month) {
                                                        echo " - ".$_monthsList[$tribe_end_month];
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="event-r">
                                            <img src="<?php echo $ev_img; ?>" class="card-list__image">
                                            <div class="event-main-city">
                                              <?php echo tribe_get_city($post->ID); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php $cnt_li++; endforeach; ?>
                            </div>
                        </div>
                        <!--
                        <div class="nav-flip">
                            <button class="prev"><img src="<?php bloginfo("template_url"); ?>/inc/img/arrow-l.svg"></button>
                            <button class="next"><img src="<?php bloginfo("template_url"); ?>/inc/img/arrow-r.svg"></button>
                        </div>
                        -->
                    </div>
                </div>
                  <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="container section top-news mt-30">
            <div class="row">
                <div class="col-lg-6 news-left">
                    <div class="news-main-big-wrap">
<?php
// main news
if ( have_posts() ) : 
query_posts('cat=25');
      while (have_posts()) : the_post(); 
         if (get_field('top_news') == 1 ) :
?>
             <div class="news-main-block-big">
                 <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                     <div class="news-main-title-big"><a
                                 href="<?php the_permalink(); ?>"><?php break_text(get_the_title(), 120); ?></a></div>
                     <div class="news-main-date-big"><?php the_time('d.m.y') ?></div>
                     <div class="news-main-preview-big"><?php break_text(preg_replace('/<[^>]*>/', '',
                             get_the_content()), 310); ?></div>
                 </div>
                 <div class="button-grey-green">
                     <a href="<?php the_permalink(); ?>">Подробнее</a>
                     <div class="news-main-block-leaf">
                     </div>
                 </div>
             </div>
             <div class="news-main-img-big" style="background: url('<?php echo the_post_thumbnail_url($id); ?>');
                     background-position: center center; background-size: cover;">
                 <div class="news-main-block-leaf">
                 </div>
             </div>
<?php
             break;
          endif;
      endwhile;
    endif;
?>
                    </div>
                </div>

                <div class="col-lg-6 news-right">
<div class="equipment-wrap">
<div id="flexslider1" class="flexslider">
  <ul class="slides">
    <li>
<div class="equipment-preview">
    <div class="equipment-title">
        <div class="equipment-txt">
Анонс оборудования
        </div>
        <div class="equipment-sign">
<img src="<?php bloginfo("template_url"); ?>/inc/img/megafon-green.svg">
        </div>
    </div>
	<div class="right-slider-wrap">
    <div class="right-slider">
        <div class="right-slider-descr">
Вертикальная автоматическая упаковочная машина, уавковывает продукт в пакет "Квадропак".
            <div class="right-slider-table">
                <div class="right-slider-param">
                <div>Тип</div>
                <div>Вид</div>
                </div>
                <div class="right-slider-param-title">
                <div>Автоматическая</div>
                <div>Упаковочное оборудование</div>
                </div>
            </div>
         </div>
        <div class="right-slider-img">
<img src="/wp-content/uploads/2020/11/slr-1.jpg">
        </div>
    </div>
  </div>
</div>
    </li>
    <li>
<div class="equipment-preview">
    <div class="equipment-title">
        <div class="equipment-txt">
Акция на упаковку
        </div>
        <div class="equipment-sign">
<img src="<?php bloginfo("template_url"); ?>/inc/img/percent-green.svg">
        </div>
    </div>
	<div class="right-slider-wrap">
    <div class="right-slider">
        <div class="right-slider-descr">
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
            <div class="right-slider-table">
                <div class="right-slider-param">
                <div>Тип упаковки</div>
                <div>Продукт</div>
                </div>
                <div class="right-slider-param-title">
                <div>Сливер</div>
                <div>Яблоки, груши</div>
                </div>
            </div>
         </div>
        <div class="right-slider-img">
<img src="/wp-content/uploads/2020/11/slr-2.jpg">
        </div>
    </div>
  </div>
</div>
    </li>
      <li>
          <div class="equipment-preview">
              <div class="equipment-title">
                  <div class="equipment-txt">
                      Презентация комбайна
                  </div>
                  <div class="equipment-sign">
                      <img src="<?php bloginfo("template_url"); ?>/inc/img/video-green.svg">
                  </div>
              </div>
              <div class="right-slider-wrap">
                  <div class="right-slider">
                      <div class="right-slider-descr-video">
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                      </div>
                      <div class="right-slider-video">
                          <div class="video-clip">
                              <iframe width="280" height="180" src="https://www.youtube.com/embed/lWjCe-SdI1g" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </li>
  </ul>
</div>
</div>

                </div>


            </div>
        </div>

        <div class="container section three-news">
            <div class="row">
<?php
// news block
if ( have_posts() ) : 
$post_column = 1;
query_posts('cat=25&posts_per_page=3');
      while (have_posts()) : the_post(); 
if ($post_column == 1) $column_paddind = " news-col-left";
else if ($post_column == 3) $column_paddind = " news-col-right";
else $column_paddind = "";
?>
                <div class="col-lg-4 news-main-block<?php echo $column_paddind?>">
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <div class="news-main-title"><a href="<?php the_permalink(); ?>"><?php break_text(get_the_title(), 146); ?></a></div>
          <div class="news-main-date"><?php the_time('d.m.y') ?></div>
          <div class="news-main-preview">
<?php break_text(preg_replace('/<[^>]*>/', '', get_the_content()), 310); ?>
          </div>
        </div>
                </div>
<?php
      $post_column++;
      endwhile;
    endif;
?>
            </div>
            <div class="row">
<div class="news-main-all-button">
  <a href="/news">Смотреть все новости</a>
</div>
            </div>
        </div>

        <div class="container section clients-top clients-top-with-map clients-main-page">
            <div class="row">
                <div class="col-lg-12 clients-before-map">
<div class="clients-wrap">
<div class="clients-title">
<h2>С нами работают</h2>
</div>
</div>
<?php echo do_shortcode('[metaslider id="956"]');?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 map-on-main">
<?php echo do_shortcode('[russiahtml5map id="3"] '); ?>
            	</div>
            </div>
        </div>

<?php
get_footer();
