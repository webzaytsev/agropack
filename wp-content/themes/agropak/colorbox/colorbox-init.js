/* Initialize jQuery Colorbox*/
jQuery(function( $ ){
 $('a[href*=".jpg"], a[href*=".jpeg"], a[href*=".png"], a[href*=".gif"]').colorbox({
    transition:'none',
    rel: 'gallery',
    opacity:.85,
    maxWidth: '95%',
    maxHeight: '95%',
 
    title: function() {
    return $(this).find('img').attr('alt');
    } 
    });
});