<?php
/**
* Template Name: Full Width
 */

get_header(); ?>
    <h1 class="proizvodstvo-h1">История компании, наш путь развития </h1>
    <div class="container section" id="warranty-block-wrap">
        <div class="row">
            <div class="col-lg-6 company-left">
                <div class="service-main-pic leaf-information">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="proizvodstvo-date">
                                1997 год
                            </div>
                            <div class="proizvodstvo-text">
                                Основание компании, формирование рынка транспортной и розничной упаковки.
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="proizvodstvo-date">
                                2002 год
                            </div>
                            <div class="proizvodstvo-text">
                                Расширение сферы деятельности, ставка на комплексность: оборудование, упаковка, сервис.                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="proizvodstvo-date">
                                2010 год
                            </div>
                            <div class="proizvodstvo-text">
                                Формирование трендов в отрасли, изучение зарубежного опыта и новый подход к упаковке.                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="proizvodstvo-date">
                                2014 год
                            </div>
                            <div class="proizvodstvo-text">
                                Запуск производства комбиполотна для упаковки - домик.
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="proizvodstvo-date">
                                2018 год
                            </div>
                            <div class="proizvodstvo-text last-bottom">
                                Запуск собственного производства оборудования для обработки и упаковки овощей и фруктов.                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="proizvodstvo-date">
                                2020 год
                            </div>
                            <div class="proizvodstvo-text last-bottom">
                                Рестайлинг компании и презентация нового направления: ПО для контроля агропроцессов.                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 company-right company-slider-text">
                <div class="service-main-block">
                    <p>Сегодня Агропак – российский производитель и эксклюзивный представитель европейского оборудования для упаковки, калибровки, логистики свежих овощей и фруктов, поставщик различных упаковочных материалов, транспортной тары, проектировщик и разработчик IT-решений для контроля производственных процессов на агропредприятии.</p>
                    <p class="green-p">За 24 года работы мы реализовали и модернизировали 700 различных сельскохозяйственных проектов, установили более 5000 единиц оборудования ведущих производителей, таких как: Ekko, Newtec, C-Pack, Gillenkirch, Greefa, Werbruggen т других.</p>
                </div>
                <div class="company-assoc">
                    <div class="clients-wrap">
                        <h2>Состоим в ассоциациях</h2>
                    </div>
                  <?php echo do_shortcode('[metaslider id="388"]'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="container section engineering-wrap">
        <h2 class="company-directions">Наша компания работает в четырех отраслевых направлениях</h2>
        <div class="equipment-main equipment-catalog">
            <div class="equip-main-wrap">
                <a href="/equipment/openground/">
                    <div class="equip-main-circle">
                        <div class="equip-main">
                            <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-1.svg" alt="Открытый грунт">
                        </div>
                    </div>
                    <div class="equip-main-link">
                        Открытый грунт
                    </div>
                </a>
            </div>
            <div class="equip-main-wrap">
                <a href="/equipment/protectedground/">
                    <div class="equip-main-circle">
                        <div class="equip-main">
                            <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-2.svg" alt="Защищенный грунт">
                        </div>
                    </div>
                    <div class="equip-main-link">
                        Защищенный грунт
                    </div>
                </a>
            </div>
            <div class="equip-main-wrap">
                <a href="/equipment/gardening/">
                    <div class="equip-main-circle">
                        <div class="equip-main">
                            <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-3.svg" alt="Садоводство">
                        </div>
                    </div>
                    <div class="equip-main-link">
                        Садоводство
                    </div>
                </a>
            </div>
            <div class="equip-main-wrap">
                <a href="/equipment//berries-fruits/">
                    <div class="equip-main-circle">
                        <div class="equip-main">
                            <img style="max-width: 75px;" src="<?php bloginfo("template_url"); ?>/inc/img/icon-equipment-4.svg" alt="Ягоды и фрукты">
                        </div>
                    </div>
                    <div class="equip-main-link">
                        Ягоды и фрукты
                    </div>
                </a>
            </div>
        </div>

        <div class="slider-category-narrow">
            <div id="flexslider-category" class="flexslider company-category">
                <ul class="slides">
                    <li>
                        <div class="equip-main-wrap">
                            <a href="/equipment/gardening/">
                                <div class="equip-main-circle">
                                    <div class="equip-main">
                                        <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-3.svg" alt="Садоводство">
                                    </div>
                                </div>
                                <div class="equip-main-link">
                                    Садоводство
                                </div>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div class="equip-main-wrap">
                            <a href="/equipment/openground/">
                                <div class="equip-main-circle">
                                    <div class="equip-main">
                                        <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-1.svg" alt="Открытый грунт">
                                    </div>
                                </div>
                                <div class="equip-main-link">
                                    Открытый грунт
                                </div>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div class="equip-main-wrap">
                            <a href="/equipment/protectedground/">
                                <div class="equip-main-circle">
                                    <div class="equip-main">
                                        <img src="<?php bloginfo("template_url"); ?>/inc/img/equip-2.svg" alt="Защищенный грунт">
                                    </div>
                                </div>
                                <div class="equip-main-link">
                                    Защищенный грунт
                                </div>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div class="equip-main-wrap">
                            <a href="/equipment//berries-fruits/">
                                <div class="equip-main-circle">
                                    <div class="equip-main">
                                        <img style="max-width: 75px;" src="<?php bloginfo("template_url"); ?>/inc/img/icon-equipment-4.svg" alt="Ягоды и фрукты">
                                    </div>
                                </div>
                                <div class="equip-main-link">
                                    Ягоды и фрукты
                                </div>
                            </a>
                        </div>
                    </li>

                </ul>
            </div>
        </div>



    </div>

    <div class="container section" id="warranty-block-wrap">
        <div class="row">
            <div class="col-lg-6 company-left clients-sl-left">
                <div class="clients-slider-title">
                    <h2>Наши клиенты</h2>
                    <div class="clients-slider-title">

                    </div>
                </div>
                <div id="flexslider-clients" class="flexslider">
                    <ul class="slides">
                        <li>
                            <div class="clients-slider-slide">
                                <div class="clients-slider-slide-logo">
                                    <img src="/wp-content/themes/agropak/inc/img/clients/prinevskoe.jpg">
                                </div>
                                <div class="clients-slider-slide-txt">
                                    Название компании в несколько строк
                                </div>
                            </div>
                            <div class="clients-slider-slide">
                                <div class="clients-slider-slide-logo">
                                    <img src="/wp-content/themes/agropak/inc/img/clients/eco-kultura.jpg">
                                </div>
                                <div class="clients-slider-slide-txt">
                                    Название компании в несколько строк
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="clients-slider-slide">
                                <div class="clients-slider-slide-logo">
                                    <img src="/wp-content/themes/agropak/inc/img/clients/ivanisovo.png">
                                </div>
                                <div class="clients-slider-slide-txt">
                                    Название компании в несколько строк
                                </div>
                            </div>
                            <div class="clients-slider-slide">
                                <div class="clients-slider-slide-logo">
                                    <img src="/wp-content/themes/agropak/inc/img/clients/emelianovskiy.jpg">
                                </div>
                                <div class="clients-slider-slide-txt">
                                    Название компании в несколько строк
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="clients-slider-slide">
                                <div class="clients-slider-slide-logo">
                                    <img src="/wp-content/themes/agropak/inc/img/clients/irrico.jpg">
                                </div>
                                <div class="clients-slider-slide-txt">
                                    Название компании в несколько строк
                                </div>
                            </div>
                            <div class="clients-slider-slide">
                                <div class="clients-slider-slide-logo">
                                    <img src="/wp-content/themes/agropak/inc/img/clients/kazachiy-hutor.jpg">
                                </div>
                                <div class="clients-slider-slide-txt">
                                    Название компании в несколько строк
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="clients-slider-slide">
                                <div class="clients-slider-slide-logo">
                                    <img src="/wp-content/themes/agropak/inc/img/clients/vyborgec.jpg">
                                </div>
                                <div class="clients-slider-slide-txt">
                                    Название компании в несколько строк
                                </div>
                            </div>
                            <div class="clients-slider-slide">
                                <div class="clients-slider-slide-logo">
                                    <img src="/wp-content/themes/agropak/inc/img/clients/nevskaya-co.jpg">
                                </div>
                                <div class="clients-slider-slide-txt">
                                    Название компании в несколько строк
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="clients-slider-slide">
                                <div class="clients-slider-slide-logo">
                                    <img src="/wp-content/themes/agropak/inc/img/clients/rost.png">
                                </div>
                                <div class="clients-slider-slide-txt">
                                    Название компании в несколько строк
                                </div>
                            </div>
                            <div class="clients-slider-slide">
                                <div class="clients-slider-slide-logo">
                                    <img src="/wp-content/themes/agropak/inc/img/clients/lukovickie-ovoshi.png">
                                </div>
                                <div class="clients-slider-slide-txt">
                                    Название компании в несколько строк
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 company-right">
                <div class="clients-slider-title">
                    <h2>Наши партнеры</h2>
                </div>
                <div id="flexslider-partners" class="flexslider">
                    <ul class="slides">
                        <li>
                            <a href="https://www.newtec.com/" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/newtec.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        Newtec A/S
                                    </div>
                                </div>
                            </a>
                            <a href="http://www.c-pack.com/" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/cpack.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        C-Pack
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.gillenkirch.com/" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/gillenkirch.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        Gillenkirch
                                    </div>
                                </div>
                            </a>
                            <a href="https://www.jasa.nl/" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/jasa.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        JASA packaking solutions
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="https://ekkoas.dk/" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/ekko.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        EKKO Maskiner A/S
                                    </div>
                                </div>
                            </a>
                            <a href="http://www.formit.fi/" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/formit.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        Formit
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.ulmapackaging.com" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/ulma.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        ULMA packaging
                                    </div>
                                </div>
                            </a>
                            <a href="https://www.frutmac.com" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/frutmac.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        Frutmac packaking solutions
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="https://webercooling.com" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/weber.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        Weber Cooling
                                    </div>
                                </div>
                            </a>
                            <a href="https://dtdijkstra.nl/" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/dijkstra.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        DT Dijkstra
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.christiaensagro.com" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/christiaens.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        Christiaens Agro Systems
                                    </div>
                                </div>
                            </a>
                            <a href="https://www.greefa.com" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/greefa.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        Greefa grading & packing
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.taks.nl/?lang=en" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/taks.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        TAKS Handling Systems
                                    </div>
                                </div>
                            </a>
                            <a href="https://unikon.com/nl/" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/unikon.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        UNIKON industrial washing machines
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="https://finis.nl/" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/finis.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        Finis foodprocessing equipment B.V.
                                    </div>
                                </div>
                            </a>
                            <a href="http://eillert.nl/en/" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/eillert.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        Eillert B.V.
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.frumacoeurope.eu/" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/frumaco.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        Frumaco Agriculture echnologies
                                    </div>
                                </div>
                            </a>
                            <a href="https://www.topcontrol.it" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/topcontrol.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        TopControl Automation and control system
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.brimapack.com/en/" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/brimapack.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        BrimaPack
                                    </div>
                                </div>
                            </a>
                            <a href="https://www.boixeurope.com/" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/boix.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        Boix Europe
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.verbruggen-palletizing.com/ru/" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/verbruggen.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        Verbruggen palletizing
                                    </div>
                                </div>
                            </a>
                            <a href="https://www.espera.com/en" target="_blank" rel="nofollow">
                                <div class="clients-slider-slide">
                                    <div class="clients-slider-slide-logo">
                                        <img src="/wp-content/themes/agropak/inc/img/partners/espera.png">
                                    </div>
                                    <div class="clients-slider-slide-txt">
                                        Espera
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container section engineering-wrap modernization-block">
        <div class="row">
            <div class="col-lg-6">
                <div class="engineering">
                    <h2>Познакомиться с реализованными проектами Агропак</h2>
                </div>
                <p class="service-last-p">
                    Агропак торгово-производственная компания. Мы разрабатываем, производим, объединяем и адаптируем упаковочные решения для производителей овощей, фруктов, яблок, грибов и салата, позволяя им добиться максимального взаимодействия и эффективности бизнес-процесса. Мы предлагаем нашим клиентам, эксклюзивные и простые решения, объединяя зарекомендовавших себя в России и мире.
                </p>
            </div>
            <div class="col-lg-6 company-projects">
                <div>
                    <img src="<?php bloginfo("template_url"); ?>/inc/img/projects-pics.png">
                </div>
                <div class="button-net">
                    <div class="button-service">
                        <a href="/projects">Изучить проекты</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
include "our-clients.php";
?>

<?php
get_footer();
